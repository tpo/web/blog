# Tor Blog

The Tor Blog is a Lektor static site generator project.

## Archiving old content

Since Lektor doesn't scale for projects with many pages, it's necessary to
reduce the number of pages built by Lektor and handle this content differently.

The way this is done here is by using the `blog_tools.py` custom script to
archive the build artifacts (HTML and attachements) of older posts and events
from the `content` directory and into `archived`.

After building the website, copying everything from `archived` into the output
directory restores the old content. This must be done separately and after the
Lektor build, otherwise Lektor automatically prunes (deletes) it.

Because Lektor doesn't know about the archived content anymore, we generate
JSON-formatted metadata for it and store it as databags. This data is used to
build the new sitemap page, which lists all blog posts and events, both
archived and non-archived.

The steps to archive content are:

- execute a build as normal, using `lektor build`
- run `./blog_tools.py --archive=<YYYY-mm-dd> --output-path $(lektor project-info --output-path)`
- commit the changes in `archived`, `content` and `databag` directories

To restore the archived contents into a subsequent build (for publishing to
`blog.torproject.org`):

- run `./blog_tools.py --post-process --output-path $(lektor project-info --output-path)`
