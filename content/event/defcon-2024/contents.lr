title: Defcon 32, Las Vegas
---
author: pavel
---
start_date: 2024-08-08
---
end_date: 2024-08-11
---
body:
DEF CON is one of the oldest continuously running hacker conventions around, and also one of the largest.

Join us at Defcon32! Come by our booth, check out new Tor merch, and listen to our talk "Measuring the Tor network" on Saturday, August 10th at 3.00pm.

Millions of people around the world use Tor every day to protect themselves from surveillance and censorship. While the Tor Browser and its protocol are widely known, the backbone of the Tor ecosystem, its extensive network of volunteer relays, is often subject to speculation and misinformation. The Tor Project is dedicated to supporting this network and fostering a vibrant, diverse community of relay operators.

This talk will focus on our efforts to maintain a healthy network and community, and detect and mitigate attacks -- all with the help of metrics and analysis of usage patterns. By illustrating how we collect safe-enough metrics for an anonymity network, we will offer insights into how we identify unusual activity and other noteworthy events on the network. We will also discuss our ongoing strategies for addressing current and future network health challenges.

If you are interested in understanding the inner workings of the Tor network and its relay community and how we keep this vital ecosystem running, this talk is for you.

For more info, visit: https://defcon.org/html/defcon-32/dc-32-speakers.html 

---
tags:

events
