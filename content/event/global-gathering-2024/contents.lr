title: Global Gathering 2024
---
author: pavel
---
start_date: 2024-09-27
---
end_date: 2024-09-29
---
body:

The event brings together digital rights networks from around the world for three days of collaboration building, knowledge sharing, brainstorming, and rejuvenation.

At the Tor and Tails booth you can learn more about tools that can help you be anonymous on the internet. Come to chat with us about how to use the Tor Browser or the Tails OS to protect your privacy online and bypass censorship. Learn more about our community and how to volunteer. And if you would like to partner with us for digital security training in your area.

We will also host a variety of circles & sessions:
- Friday September 27
 * 15:00 Working on the well-being of workers - discussion about policies for well-being of workers and how to build democratic processes within the organization. -- Isabela
 * 16:30  Running Tor relays in/by the Global South - session on how to run Tor relays in people's own contexts, technical requirements, to motivate people in Global South countries to run relays. --Raya

- Saturday September 28
 * Developing robust signalling channel – For users in highly censored regions, gaining
access to anti-censorship tools can be difficult since these tools are prime targets of blocking by censors. A signalling channels is a
highly available (i.e., difficult for censors to block), typically low-bandwidth, method of communication that aims to provide users access to more robust anti-censorship tools. Typically a user makes a request to a signalling channel and gets a response that includes secret keys and/or addresses of proxies or bridges for users to make a censorship-resistant connection. Despite the existence of many censorship-resistant tools, options for signalling channels are limited. With many cloud providers opting to discontinue support for domain fronting, we are interested in exploring strategies around new signalling channels that might be able to help censored users get essential information to make anti-censorship connections.

- Sunday September 29
 * Leveraging Weblate and automated checks for higher quality localization – In this session we will share best practice on localizing Internet Freedom applications and we will share how Tor Project migrated from Transifex into Weblate, a free software project.
 * Tor Browser, Tails, and You – How do Tails and the Tor Browser currently fit into the digital security toolbox for your communities? How could they become more helpful in that toolbox? Come and discuss it with the Tor Applications Team and the Tails Team.

 For more info on programming, please visit: https://wiki.digitalrights.community/index.php?title=Global_Gathering_2024

---
tags: events
