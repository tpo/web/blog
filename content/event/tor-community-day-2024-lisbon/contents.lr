title: Tor Community Day 2024/ Dia da Comunidade do Tor de 2024
---
author: pavel
---
start_date: 2024-05-25
---
end_date: 2024-05-25
---
body:

More event info here: https://blog.torproject.org/tor-community-day-2024/

### When
- Date: May 25th, 2024
- Time: 9:00 AM to 5:00 PM. (There will be an hour and a half break for lunch.)

### Where
- Location: Universidade NOVA de Lisboa, Colégio Almada Negreiros, Room 209.

### Who
- Organizers: The Tor Project and PrivacyLx
- The Tor Community Day is free and open to the public. Anyone who wishes to join is welcome without any prior need to sign up. If you're curious about Tor, stop by!

### Program
- This is a bilingual event: The first half of the event will be in English, the second half in Portuguese.
- We'll have talks from the Tor Project and from members of the community in Portugal.
- Be on the lookout for updates about the schedule and participating projects on our social channels and the Tor forum.

- 09h00 - 09h15: Welcome by Kevin and Meskio
- 09h15 - 10h00: Workshop: How to use Tor by Juga
- 10h00 - 10h15: Break and discussion
- 10h15 - 10h30: Run a Tor relay at your University by Roger
- 10h30 - 11h00: Academic work in anti-censorship by Afonso
- 11h00 - 11h15: Break and discussion
- 11h15 - 11h45: How to report a bug by Emma
- 11h45 - 12h15: Localization Work in Tor by Cacu
- 12h15 - 14h15: Lunch break (Lunch not provided)
- 14h15 - 14h30: Como funciona o Tor por Kevin
- 14h30 - 15h00: Snowflake por Gus
- 15h00 - 15h30: Pausa e conversa
- 15h30 - 16h00: Experiência de correr 30 bridges do Snowflake por Cisco e Vasilis
- 16h00 - 16h30: Pausa e conversa
- 16h30 - 17h00: Serviços de Onion por Rhatto
- 17h00: Encerramento do evento por Kevin e Meskio


### Quando
- Data: 25 de maio de 2024
- Horário: 9h às 17h. (Haverá um intervalo de uma hora e meia para o almoço).

### Onde
- Localização: Universidade NOVA de Lisboa, Colégio Almada Negreiros, Sala 209.

### Quem
- Organizadores: The Tor Project e PrivacyLx
- O Dia da Comunidade do Tor é gratuito e aberto ao público. Qualquer pessoa que deseje participar é bem-vinda sem necessidade de se inscrever previamente. Se estiveres curioso sobre o Tor, aparece!

### Programa
- Este é um evento bilingue: A primeira metade do evento será em inglês, a segunda metade em português.
- Teremos palestras do Projeto Tor e de membros da comunidade em Portugal.
- Estejam atentos a actualizações sobre a programação e os projectos participantes nos nossos canais sociais e no fórum Tor.

---
tags: events
