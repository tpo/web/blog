title: New Release: Tor Browser 13.5.3
---
pub_date: 2024-09-04
---
author: morgan
---
categories:

applications
releases
---
summary: Tor Browser 13.5.3 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.5.3 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.5.3/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.5.2](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.5/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated NoScript to 11.4.35
  - [Bug tor-browser#40056](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40056): Ensure that the lazy loading attribute is ignored on script-disabled documents
  - [Bug tor-browser#42686](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42686): Backport Mozilla 1885101
  - [Bug tor-browser#42829](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42829): Prevent CSS-based scriptless interaction tracking
  - [Bug tor-browser#43084](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43084): Rebase Tor Browser Stable onto 115.15.0esr
  - [Bug tor-browser#43100](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43100): Backport security fixes from Firefox 130
  - [Bug tor-browser-build#41207](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41207): Upgrade lyrebird to 0.3.0
- Windows + macOS + Linux
  - Updated Firefox to 115.15.0esr
  - [Bug tor-browser#42596](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42596): Several console errors: Console.maxLogLevelPref used with a non-existing pref:
  - [Bug tor-browser#42622](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42622): Offline state is unreachable in about:torconnect (first bootstrap attempt)
  - [Bug tor-browser#42642](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42642): Downloads button warning no longer announced on Orca
  - [Bug tor-browser#42661](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42661): Re-run update_emojis.py and update locales
  - [Bug tor-browser#42691](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42691): Simplified bridge cards prevent censored users from modifying built-in bridges
  - [Bug tor-browser#42696](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42696): Update `mail` icon used in "Find more bridges"
  - [Bug tor-browser#42697](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42697): Remove padding to left of `tor-bridges-provider-list` under "Find more bridges"
  - [Bug tor-browser#43059](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43059): Drag and Drop issue in new update 13.5.2
  - [Bug tor-browser#43066](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43066): about:torconnect no longer changes the title icon on errors
- Linux
  - [Bug tor-browser#43064](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43064): Make copy/paste and drag/drop file filtering more specific
- Android
  - Updated GeckoView to 115.15.0esr
- Build System
  - All Platforms
    - Updated Go to 1.21.13
    - [Bug tor-browser-build#41213](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41213): Update the update_manual.py script to notify when no changes needed
    - [Bug tor-browser-build#41218](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41218): Use new Tor Browser gpg subkey for signing stable releases
    - [Bug tor-browser-build#41222](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41222): link_old_mar_filenames still referenced in torbrowser-incrementals-{release,alpha}-unsigned
  - Android
    - [Bug tor-browser-build#41206](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41206): GeckoView ignores the number of processors
