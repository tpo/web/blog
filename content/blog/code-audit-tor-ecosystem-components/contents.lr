title: Code audit for the Tor Project completed by Radically Open Security
---
author: pavel
---
pub_date: 2024-01-29
---
categories: 

reports
applications
---
summary: [Radically Open Security](https://www.radicallyopensecurity.com/) conducted a comprehensive code audit for several components of the Tor ecosystem. This blog post outlines key recommendations and links to the full report.
---
body:

Between April 17, 2023, and August 13, 2023, [Radically Open Security](https://www.radicallyopensecurity.com/) conducted a comprehensive code audit for the Tor Project, including reporting and optional retesting.

The code audit focused on several components of the Tor ecosystem:
- Tor Browser and Tor Browser for Android,
- Exit relays (Tor core), 
- Exposed services (metrics server, SWBS, Onionoo API), 
- Infrastructure components (monitoring & alert), and testing/profiling tools. 

The primary objective was to assess software changes made to improve the Tor [network's speed and reliability](https://blog.torproject.org/introducing-proof-of-work-defense-for-onion-services/) and a number of recommendations were made such as:

- Reducing the potential attack surface of the public-facing infrastructure, 
- Addressing outdated libraries and software, 
- Implementing modern web security standards, 
- And following redirects in all HTTP clients by default. 

Additionally, fixing issues related to denial-of-service vulnerabilities, local attacks, insecure permissions, and insufficient input validation was deemed imperative.

We would like to thank Radically Open Security for performing the audit and the U.S. State Department Bureau of Democracy, Human Rights, and Labor (DRL) for sponsoring this project and ['Making the Tor network faster & more reliable for users in Internet-repressive places’](https://gitlab.torproject.org/tpo/team/-/wikis/sponsors-2022#making-the-tor-network-faster-more-reliable-for-users-in-internet-repressive-places-s61).

### For more details and information, please access [the complete audit report here](The Tor Project Pentest Report 2023 1.0).