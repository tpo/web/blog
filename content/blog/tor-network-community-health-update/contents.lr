title: Safeguarding the Tor network: our commitment to network health and supporting relay operators
---
author: isabela
---
pub_date: 2023-11-20
---
categories:

network
relays
---
summary: In this blog post, we want to reaffirm our commitment to keeping Tor free and provide insight into the rationale behind the recent removal of certain bad relays.
---
body:

Some of you may have noticed that we have recently removed a large number of relays from the Tor network. We did this to protect network health and the safety and security of our community and users. This process has sparked larger discussions within our teams and community members about the state of Tor relay policies and the potential for incentivization models that can better support our relay operators and the growth of the Tor network. It has also encouraged us to reflect more deeply on our mission and the role of free, open-source technology.

In this blog post, we want to reaffirm our commitment to keeping Tor free, and provide insight into the rationale behind our recent actions to protect the network from bad actors.

## The problem with for-profit schemes

As a free and open-source project, the Tor network is built on the principles of openness, collaboration, and community-driven development. It's powered by a community that relies on the support of volunteers who donate their resources and bandwidth to ensure that users around the world can access the free and open internet. This gives our users and the larger community the freedom to access, run, co-develop, and share the software without being locked into a particular vendor, platform, or service provider.

This openness makes our network (and the privacy guarantees it offers) more robust and resilient to attacks. But this openness can also [open up avenues for malicious operators](https://blog.torproject.org/malicious-relays-health-tor-network/). This is why we have put in place [relay requirements](https://community.torproject.org/relay/relays-requirements/), [relay policies](https://community.torproject.org/policies/relays/evaluation-criteria-for-solution-to-combat-malicious-relays/) and [criteria for rejecting bad relays](https://gitlab.torproject.org/tpo/network-health/team/-/wikis/Criteria-for-rejecting-bad-relays) to ensure the overall health of our network, safety of our users, and alignment with our values.

### Considering incentives to grow the Tor network

The discussions about possible incentives for relay operators go as far back as the creation of the Tor network. Which ones might align with the values and intrinsic motivators of the Tor community? And why do externally mediated rewards and monetary compensation pose a threat?

We have worked with researchers to design and evaluate different models, and published papers and blog posts outlining the wide range of potential issues with rewards, including a possible TorCoin. [From an over-reliance on a central entity,](https://blog.torproject.org/tor-incentives-research-roundup-goldstar-par-braids-lira-tears-and-torcoin/) which could put user anonymity at risk in designs that prioritize some traffic, to [legal classification and liability concerns](https://blog.torproject.org/two-incentive-designs-tor/) that would arise from the introduction of real money to loss of location diversity and many more. So far, none of the proposed schemes has been able to deliver a comprehensive and safe solution to address the issue of scale.

All of this is not to say that it cannot be done. In fact, this research has significantly advanced those discussions and introduced a number of requirements that a well-designed incentive scheme "should fulfill: [both clients and relays should be able to receive the incentive so that neither group stands out; and the timing of payments should not allow cheating and should not enable linkability."](https://blog.torproject.org/tor-incentives-research-roundup-goldstar-par-braids-lira-tears-and-torcoin/) However, these efforts also strongly indicate that there isn't a shortcut, no third-party silver bullet---and that more work is needed. Above all, that this work needs to happen in collaboration and partnership with the Tor Project and our community of directory authorities and relay operators. In fact, we hope to ["motivate new researchers and developers to start thinking about solutions to problems that Tor is interested in"](https://blog.torproject.org/tor-incentives-research-roundup-goldstar-par-braids-lira-tears-and-torcoin/) and continue to [submit ideas and proposals to the network health team.](https://community.torproject.org/policies/relays/001-community-relay-operator-process/).

## Recent threats to Tor network health

Recently, we've identified some operators associated with a high-risk, for-profit scheme. This financial scheme is promising monetary gains with cryptocurrency tokens, and is operated by third parties without the endorsement or approval of The Tor Project. We consider these relays to be harmful to the Tor network for a number of reasons, including that certain of the relays do not meet our requirements, and that such financial schemes present a significant threat to the network's integrity and the reputation of our project as they can attract individuals with malicious intent, put users at risk, or disrupt the volunteer-driven spirit that sustains the Tor Community.

As part of our assessment and due diligence into the matter, we engaged with relay operators and were often presented with scenarios in which relay operators associated with this scheme were putting themselves at risk by lacking the awareness of what project they were actually contributing to or operating relays in unsafe or high-risk regions. It has become clear to us that this scheme is not beneficial to the Tor network or the Tor Project. Which is why we proposed the rejection of those relays to our directory authorities who voted in favor of removing them.

This recommendation is further rooted in the fundamental principles of the Tor network: collaboration, the commitment to fight internet censorship and pervasive surveillance---and having the highest priority be to safeguard people's access to privacy and anonymity online. By removing relays associated with this for-profit scheme, the Tor network not only protects its users from potential harm but also reinforces its commitment to maintain a trusted and community-driven network. Upholding these principles is essential to ensure that Tor remains a safe and reliable tool for users seeking privacy and anonymity online.
## Free and open-source means putting power in the hands of our community

Free, open-source, and distributed solutions like Tor help keep us connected when corporate and government interests change. When a site or a service lives in the centralized hands of a few, it's easy---perhaps inevitable---to lose control of spaces and data people create based on the changing interests of corporate and financial structures. Platforms that people rely on can change hands, direct messages can be bought and sold, and social safeguards can be eradicated.

It is essential to draw a clear distinction between genuine [donations](https://donate.torproject.org/) and contributions made in support of the Tor Project's mission. Examples of such projects that we publicly endorse include ['Relay Associations'](https://community.torproject.org/relay/community-resources/relay-associations/), a growing list of non-profit organizations around the world that allow donors to directly fund the creation of new, safe and compliant relays to grow the Tor network. EFF's [Tor University Challenge](https://toruniversity.eff.org/) is a similar initiative that aligns well with our mission.

# What's next?

The Tor network and the Tor Project's ecosystem of applications and services is here to stay and will remain free. We are ready to meet the challenges of the future of our digital rights as a community, and Tor would truly not be possible without the contributions of our volunteers---your time, bandwidth, translations, and passion.

The Tor Community and Network Health teams are making every effort to preserve the integrity of the network and uphold the values that have been at its core since its inception. 

To provide better support and governance for our relay operator community, we have published the [process to have a proposal approved](https://community.torproject.org/policies/relays/001-community-relay-operator-process/). This is designed to encourage collaboration and drive consensus among our diverse group of supporters and stakeholders.

We are dedicated to [transparency about how we arrive at decisions pertaining to the health of the Tor network and Tor community](https://blog.torproject.org/malicious-relays-health-tor-network), and are open to feedback where we can do better. There are many ways to participate, voice concerns, and get engaged, such as attending our [monthly relay operator meetups and connecting with people on our forum](https://forum.torproject.org/c/support/relay-operator/17).

We also acknowledge the need to evolve and expand the support systems for our relay operators. We are diligently evaluating any and all ideas or proposals that are proactively brought to us. Only then can we scale responsibly without undermining the privacy protections that Tor offers to our millions of users.
