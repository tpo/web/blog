title: Thank you! Our 2023 year-end fundraising results 🎉
---
author: alsmith
---
pub_date: 2024-02-14
---
categories:
fundraising
---
weight: 1
---
summary: Today I am happy to share the results of the Tor Project’s 2023 year-end fundraising campaign. From October through December 2023, **you answered the call for support** by contributing $427,558 to power the Tor network, Tor Browser, onion services, Snowflake, and the ecosystem of tools and services built and maintained by the Tor Project.
---
body:

Good news, Tor community! 

Today I am happy to share the results of the Tor Project’s 2023 year-end fundraising campaign.

From October through December 2023, **you answered the call for support** by contributing $427,558 to power the Tor network, Tor Browser, onion services, Snowflake, and the ecosystem of tools and services built and maintained by the Tor Project.

**Everyone in our community deserves a big THANK YOU for supporting the Tor Project during the campaign.** Whether you made a donation, shared the campaign on social media, or spread the world about the importance of using Tor, you have made our success this year possible. [Your impact was then amplified by the Friends of Tor](https://blog.torproject.org/friends-of-tor-match-2023/) who provided the generous match during the campaign. 

# How will the Tor Project spend the money raised?
* **Defending access to information during the Year of Democracy.** In 2024, [more people are living in a place with a national election than ever before](https://blog.torproject.org/2024-defend-internet-freedom-during-elections/). Over the last decade, we've seen that election events bring spikes in internet censorship -- and we're expecting that these tactics of suppressing speech will be used widely this year. We’ve already begun to use funds raised to prepare. We're monitoring elections as they take place and watching for events of censorship. We're holding trainings on how to use Tor to bypass censorship to relevant communities. Not all of this work is funded by grants, and our community team relies on your contributions to make this work possible and reach more at-risk users.
* **Increasing our capacity to achieve our mission.** As the Tor Project has matured into a human-rights focused organization, we have grown to meet the challenges of an increasingly restrictive internet landscape. We share the ambitious vision of a world where everyone is able to exercise their right to privacy and access information freely. To achieve this vision, we need to have the proper infrastructure, staff support, and tools to be effective. That includes things like hiring an additional project manager, improving our donation infrastructure, and investing in tools that will help our team focus on reaching more users in real-time, support our community in moments of crises, and increase adoption of Tor technology globally. Your donation keeps the Tor Project a strong organization that can continue to build vital privacy and anti-censorship tools.
* **Expanding global access to Tor.** Every initiative to help more people use Tor is powered in part by individual donations. Your contributions will go towards expanding our live [user support channels](https://blog.torproject.org/meeting-you-where-you-are-we-added-whatsapp-user-support/), [localization](https://community.torproject.org/localization/), [training with community partners](https://blog.torproject.org/furthering-our-mission-in-the-global-south/), and tracking the development of internet freedom around the world. You’ll also be supporting onion service adoption by [continuing our development of administration tools for onion service operators](https://blog.torproject.org/how-we-plant-and-grow-new-onions/) and support of [organizations that want to release onion sites](https://blog.torproject.org/amnesty-international-launches-onion-service/); strengthening the Tor network against attacks in [our multi-year focus on reducing malicious relay activity on the network](https://gitlab.torproject.org/tpo/team/-/wikis/sponsors-2023#combating-malicious-relays-s112); and [re-writing Tor in Rust](https://blog.torproject.org/arti_1_1_11_released/), a safer, more modern language that makes Tor easier to integrate in a variety of applications and services. 

You can trust that your donations are hard at work – and not just because we say so. The Tor Project is a Charity Navigator [Four-Star Charity](https://www.charitynavigator.org/ein/208096820), and we’ve earned Candid’s [Platinum Seal of Transparency](https://www.guidestar.org/profile/shared/7d68a7b7-2a9f-4613-a985-1638da87abdf). Plus, you can read in detail about all of our revenue and expenses in our [annual fiscal year transparency reports](https://blog.torproject.org/transparency-openness-and-our-2021-and-2022-financials/).

We look forward to our continued collaboration with you, our community, to stand up for the human right to privacy, freedom of expression, and access to information. [Keep up with progress and get the latest Tor news by subscribing to our newsletter](https://newsletter.torproject.org). 