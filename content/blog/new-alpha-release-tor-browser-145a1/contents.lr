title: New Alpha Release: Tor Browser 14.5a1
---
pub_date: 2024-11-26
---
author: ma1
---
categories:

applications
releases
---
summary: Tor Browser 14.5a1 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 14.5a1 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/14.5a1/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

We would like to thank the following community members for their contribution to this release:

- NoisyCoil for several patches allowing us to create aarch64 Linux builds of our browsers,
  - [tor-browser-build#41142](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41142)
  - [tor-browser-build#41306](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41306)
  - [tor-browser-build#41307](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41307)
  - [tor-browser-build#41266](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41266)
- cypherpunks1 for Tor Browser UI improvements on Android,
  - [tor-browser#43241](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43241)
  - [tor-browser#43251](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43251) 

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 14.0a9](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated NoScript to 11.5.2
  - Updated Tor to 0.4.8.13
  - [Bug tor-browser#41710](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41710): Refactor about:torconnects relation to TorConnectParent
  - [Bug tor-browser#42125](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42125): lock RFP part 2
  - [Bug tor-browser#43307](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43307): Rebase Tor Browser alpha onto 128.5.0esr
  - [Bug tor-browser#43313](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43313): Backport security fixes from Firefox 133
- Windows + macOS + Linux
  - Updated Firefox to 128.5.0esr
  - [Bug tor-browser#42186](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42186): Drop about:tbupdate
  - [Bug tor-browser#42597](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42597): Lox.generateInvite does not convert JSON object to string
  - [Bug tor-browser#42739](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42739): Fix localization in the profile error dialog
  - [Bug tor-browser#42802](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42802): Make use of `:has` CSS selector
  - [Bug tor-browser#43237](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43237): Tweak Tor circuit display panel for screen readers
  - [Bug tor-browser#43262](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43262): Onion keys dialog. "Remove" removes all keys, "Remove all" does nothing.
  - [Bug tor-browser#43263](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43263): Onion site keys: add some alerts for screen readers
  - [Bug tor-browser#43294](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43294): Replace `Actor` `willDestroy` with `didDestroy`
  - [Bug tor-browser#43314](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43314): Tidy up connection preferences for screen readers and keyboard users
- macOS
  - [Bug MozBug 1768724#43165](https://gitlab.torproject.org/tpo/applications/MozBug 1768724/-/issues/43165): Disable Microsoft SSO on macOS [tor-browser]
- Linux
  - [Bug tor-browser#41786](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41786): Remove old fontconfig stuff at the next watershed update
  - [Bug tor-browser#41799](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41799): Make lack of fonts.conf less of a footgun
  - [Bug tor-browser-build#41298](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41298): Remove `--detach` parameter from .desktop files
  - [Bug tor-browser-build#41312](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41312): Remove comment in start-browser about --class and --name parameters
  - [Bug tor-browser-build#41313](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41313): Show waiting cursor while app opens
- Android
  - Updated GeckoView to 128.5.0esr
  - [Bug tor-browser#43232](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43232): Make the Android Meek transport easier to debug
  - [Bug tor-browser#43241](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43241): Improve hiding non-private tab features on Android
  - [Bug tor-browser#43251](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43251): Enable tab suggestions and autocomplete for private tabs on Android
- Build System
  - All Platforms
    - Updated Go to 1.22.9
    - [Bug tor-browser#43272](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43272): Fix git fetch in translation CI
    - [Bug tor-browser#43295](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43295): Update MR templates
    - [Bug tor-browser-build#40996](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40996): Do not version the .nobackup files
    - [Bug tor-browser-build#41279](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41279): Add @pierov and @ma1 as new signers
    - [Bug tor-browser-build#41284](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41284): Update relprep.py script to not synchronise changelogs between channels
    - [Bug tor-browser-build#41288](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41288): Use exec_noco option when using exec
    - [Bug tor-browser-build#41289](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41289): Fix single-browser in relprep.py
    - [Bug tor-browser-build#41300](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41300): Add bea, clairehurst, and jwilde to tb_builders
    - [Bug tor-browser-build#41304](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41304): Add a browser commit tag+signing script
    - [Bug tor-browser-build#41306](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41306): Container dependencies are sorted before resolving templates
    - [Bug tor-browser-build#41307](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41307): Container dependencies are not filtered for duplicates
    - [Bug tor-browser-build#41321](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41321): Update PieroV's expired keys
    - [Bug rbm#40006](https://gitlab.torproject.org/tpo/applications/rbm/-/issues/40006): Add option to avoid doing a git checkout when using the exec template function
  - Windows + macOS + Linux
    - [Bug tor-browser-build#41286](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41286): Update the deploy update scripts to optinally take an override hash
  - Linux
    - [Bug tor-browser-build#41142](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41142): Complete the toolchain for linux-aarch64
    - [Bug tor-browser-build#41266](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41266): Build the Tor and Mullvad Browsers for aarch64 Linux
    - [Bug tor-browser-build#41282](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41282): Add SSL to our custom Python for MozBug 1924022
