title: Resistance, change, & freedom: powered by privacy
---
author: alsmith
---
pub_date: 2022-10-13
---
categories: fundraising
---
summary: Every year, the Tor Project (a 501(c)(3) nonprofit) holds a fundraiser where we ask for your support. This year, we’re highlighting three positive forces in the world that are powered by privacy: resistance, change, and freedom.
---
body:

Every year, the Tor Project (a 501(c)(3) nonprofit) holds a [fundraiser](https://torproject.org/donate/donate-bp1-pbp) where we ask for your support. (If you’re a Tor fan, you probably know that by now!) And every year, we unveil new gifts and a slogan that highlight our values and the importance of Tor.

This year, we’re highlighting three positive forces in the world that are powered by privacy: **resistance, change, and freedom**.

We’re living in a time of rising authoritarianism. Restrictive [internet censorship and blackouts are ](https://freedomhouse.org/report/freedom-net/2021/global-drive-control-big-tech)[normal tools used by governments](https://freedomhouse.org/report/freedom-net/2021/global-drive-control-big-tech) to disrupt organizing and dissent. Companies continuously [surveill their workers](https://www.theguardian.com/commentisfree/2021/sep/16/tattleware-employers-spying-working-home) with digital spyware. Schools [restrict, monitor, and catalog student activity ](https://cdt.org/wp-content/uploads/2021/09/Student-Activity-Monitoring-Software-Research-Insights-and-Recommendations.pdf)and behavior. Data brokers [sell extremely precise browsing and location data](https://www.eff.org/press/releases/data-broker-helps-police-see-everywhere-youve-been-click-mouse-eff-investigation)–and governments easily circumvent warrants by scooping that data up for a bargain. After the 2022 reversal of Roe v Wade, U.S. [courts have already successfully used digital evidence, like web searches](https://www.washingtonpost.com/technology/2022/07/03/abortion-data-privacy-prosecution/) to prosecute people seeking abortions. All of these violations of our privacy make it difficult to exercise our human right to assemble, research, speak, and protest.

At the Tor Project, our mission is to advance human rights and freedoms with open source privacy technology, because protecting privacy is key in the work for a better world.

We’re a small nonprofit facing global adversaries. Compared to the billion dollar surveillance capitalism industry and global governments, we’re a small force. But just because we’re small doesn’t mean we can’t grow, and it doesn’t mean we can’t be successful.

**Your support is key to Tor's success, and the best way to power Tor into the future is through a [monthly ](https://torproject.org/donate/donate-bp1-pbp)[commitment](https://torproject.org/donate/donate-bp1-pbp).**

Looking at leadership organizations like EFF and Wikimedia Foundation, you can see that recurring donations–by individuals and by companies–are what allow communities to take on ambitious projects (like, say, offering privacy online to everyone who needs it!). Why does a recurring commitment matter so much? Because reliable resources mean that we can plan for the future with confidence. When we bring new folks in to work on critical areas at Tor, we want to make sure they are here for the long term. Stability like this allows us to invest in solving the big problems.

There’s a unique strength to Tor: **this community knows the power of your support to change the course of history**. In moments of need, you step up in creative ways to support Tor’s work, and millions of people can access Tor because of your drive to advance internet freedom.

![Screenshot of a Tweet reading "To help -> go to snowflake.torproject.org," with two images. The left image shows a wide shot of a protest, with people holding signs. The right image shows a close shot of one cardboard protest sign that reads, "To help -> go to snowflake.torproject.org"](twitter-screengrab.png)

You can see examples of your success right now: from spinning up more than 80,000 new Snowflakes to keep Iranians online during the #زن_زندگی_آزادی / #MahsaAmini protests to adding thousands of new bridges when Russia began censoring the Tor Project’s website.

In today’s world, Tor needs to be available for those who need it at a moment’s notice. Circumstances change and governments make decisions that upend our lives. Your commitment to Tor ensures that is the case.

**[Now through the end of the year, your donation to the Tor Project today will](https://torproject.org/donate/donate-bp1-pbp)[ be matched](https://torproject.org/donate/donate-bp1-pbp)[, 1:1](https://torproject.org/donate/donate-bp1-pbp)**. That means that every donation, up to $100,000, will be doubled. Now is an excellent time to give because your donation counts twice. Be sure to check out [https://donate.torproject.org](https://donate.torproject.org) for this year’s new t-shirt, and a brand-new offering: a sturdy canvas tote bag.

![Image of a black t-shirt and black canvas tote with a purple background. The t-shirt and tote read, "Powered by privacy; RESISTANCE, CHANGE, FREEDOM" in lime green and pink text."](yec-combo-dark-newsletter.png)

## Ways to make a difference:
* **Can you make a recurring commitment?** [Set up a monthly donation](https://torproject.org/donate/donate-bp1-pbp).
* **Do you have a company or organization that uses Tor or believes in Tor?** [Become a member](https://torproject.org/about/membership).
* **Does your company match donations made by employees?** Make a donation and file the paperwork with your organization so they match your gift.
* **Do you have cryptocurrency to give?** [We can accept donations in ten different coins](https://donate.torproject.org/cryptocurrency).

## 1:1 Match provided by Friends of Tor
Now, take a moment to meet the **Friends of Tor**, the generous donors who are matching your donations, up to $100,000:

![Aspiration logo](/static/images/blog/inline-images/friends-of-tor-aspiration-2021.png "class=align-left") **Aspiration** connects nonprofit organizations, foundations and activists with free and open software solutions and technology skills that help them better carry out their missions. We want those working for social and racial justice to be able to find and use the best tools and practices available, so that they maximize their effectiveness and impact and, in turn, change the world. We also [work with free and open source projects and communities in both support and partnership roles](https://aspirationtech.org/services/openprojects), advising and contributing on matters of strategy, sustainability, governance, community health, equity and diversity. We [design and facilitate unique and collaborative nonprofit and FLOSS technology convenings](https://aspirationtech.org/events), and have run almost 700 in over 50 countries as well as online over the past 16 years.

----

![Portrait of John Callas](/static/images/blog/inline-images/friends-of-tor-jon-2021.png "class=align-left")**[Jon Callas](https://twitter.com/joncallas)** is a cryptographer, software engineer, user experience designer, and entrepreneur. Jon is the co-author of many crypto and security systems including OpenPGP, DKIM, ZRTP, Skein, and Threefish. Jon has co-founded several startups including PGP, Silent Circle, and Blackphone. Jon has worked on security, user experience, and encryption for Apple, Kroll-O'Gara, Counterpane, and Entrust. Before coming to the EFF, Jon was a technologist in the ACLU's Speech, Privacy, and Technology Project on issues including surveillance, encryption, machine learning, end-user security, and privacy.  Jon is fond of Leica cameras, Morgan sports cars, and Birman cats. Jon's photographs have been used by Wired, CBS News, and The Guggenheim Museum.

----

![Portrait of Craig Newmark](/static/images/blog/inline-images/friends-of-tor-craig-2021.jpg "class=align-left")**[Craig Newmark](https://twitter.com/craignewmark)** is a Web pioneer, philanthropist, and leading advocate. Most commonly known for founding the online classified ads service craigslist, Newmark works to support and connect people and drive broad civic engagement. In 2016, he founded Craig Newmark Philanthropies to advance people and grassroots organizations that are “getting stuff done” in areas that include trustworthy journalism & the information ecosystem, voter protection, women in technology, and veterans & military families. At its core, all of Newmark’s philanthropic work helps to strengthen American democracy by supporting the values that the country aspires to – fairness, opportunity, and respect.

----

![Portrait of Wendy Seltzer](/static/images/blog/inline-images/friends-of-tor-wendy-2021.png "class=align-left")**[Wendy Seltzer](https://twitter.com/wseltzer)** is Strategy Lead and Counsel to the World Wide Web Consortium (W3C) at MIT, improving the Web's security, availability, and interoperability through standards. As a Fellow with Harvard's Berkman Klein Center for Internet & Society, Wendy founded the Lumen Project (formerly Chilling Effects Clearinghouse), the web's pioneering transparency report to measure the impact of legal takedown demands online. She seeks to improve technology policy in support of user-driven innovation and secure communication.

----

We would also like to thank **anonymous donors** who collaborated to create this fund. This spot is dedicated to them as a small recognition of their support. As the Tor community knows, anonymity loves company, so [why not join our anonymous donors and make a contribution](https://torproject.org/donate/donate-bp1-pbp), too? With their matching donation, your contribution has double the impact.
