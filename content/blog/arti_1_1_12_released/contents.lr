title: Arti 1.1.12 is released: Now you can test onion services!
---
author: nickm
---
pub_date: 2024-01-09
---
categories: announcements
---
summary:

Arti 1.1.12 is released and ready for download.
---
body:

Arti is our ongoing project to create a next-generation Tor client in
Rust.   Now we're announcing the latest release, Arti 1.1.12.

With Arti 1.1.12, it's finally possible
to run onion services for testing and experimentation.
There are a lot of rough edges and missing security features,
so we don't (yet) recommend Arti onion services for production use,
or for any purpose that requires privacy.

For instructions on how to run an onion service in Arti,
see our [work-in-progress HOWTO document](https://gitlab.torproject.org/tpo/core/arti/-/blob/arti-v1.1.12/doc/OnionService.md?ref_type=tags).
We hope to make these instructinos simpler and better
as our implementation improves.

For full details on what we've done, and for information about
many smaller and less visible changes as well,
please see the [CHANGELOG].

There's still more work to do before we move on from
basic functionality to onion service security work;
You can find a list of what we still need to do
[on the bugtracker].
Once that work is done (or "done enough")
we plan to move on to the [security features]
necessary for a private onion service implementation.

For more information on using Arti, see our top-level [README], and the
documentation for the [`arti` binary].

Thanks to everybody who's contributed to this release, including
Alexander Færøy, Dimitris Apostolou, Emil Engler, and Jim Newsome.

Also, our deep thanks to [Zcash Community Grants] and our [other sponsors]
for funding the development of Arti!

[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti
[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti#arti-1112-9-january-2024
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[other sponsors]: https://www.torproject.org/about/sponsors/
[on the bugtracker]: https://gitlab.torproject.org/tpo/core/arti/-/issues/?label_name%5B%5D=Onion%20Services%3A%20Basic%20Service
[security features]: https://gitlab.torproject.org/tpo/core/arti/-/issues/?label_name%5B%5D=Onion%20Services%3A%20Improved%20Security