title: The Tor Project 2020-2021 Annual Report
---
author: alsmith
---
pub_date: 2022-06-16
---
categories: reports
---
summary: On behalf of the team, I am thrilled to share with you the Tor Project's [latest Annual Report](https://www.torproject.org/static/findoc/2020-2021-TorProject-Annual-Report.pdf)!
---
body:

Hello Tor community,

On behalf of the team, I am thrilled to share with you the Tor Project's [latest Annual Report](https://www.torproject.org/static/findoc/2020-2021-TorProject-Annual-Report.pdf)! 

In the last year, the Tor community has mobilized against increasing global censorship, more than doubled the number of bridges on the Tor network (!!), and responded to tens of thousands of user support requests. We've also worked hard to make Tor faster, improve the health of our network, and connect with our community through training and events.

One element of this year's work that inspires me, and shows the power of the Tor community, is the response to the internet censorship in Russia and Ukraine. The entire Tor community immediately jumped into action to keep people online. Seeing this passion in action, while keeping tens of thousands of Russians connected to the open internet, has been inspiring. This anonymous user shared their story about Tor, which underlines the importance of our anti-censorship efforts:

> "Tor helped me a lot. Here in Russia, blocking on the Internet is extremely common... Tor helps me bypass blocking and get more privacy. For example, many wonderful websites, such as foreign services or the websites of the Russian opposition, have been blocked. I have been using Tor for many years... without it, many very important sources of useful information would be inaccessible, or accessible with great difficulty."

I hope you take a moment to read more about mobilizing against internet censorship in this year's annual report (page 4), alongside other achievements this year. **Thank you for making Tor and the freedom it provides online possible.**

Beyond the programatic accomplishments, I hope you also take a look at page 12, where we share the Tor Project's expenses and revenue for the 2020-2021 financial year, based on our audited 990 tax returns. We are proud to highlight that 87% of our expenses are releated to programmatic costs. That means that a significant majority of our expenses are directly releated to building Tor, improving Tor, and ensuring that Tor is accessible to everyone.

Please take a look at our Annual Report, let us know what you think about what we've shared, and while you're at it, check out the whole [reports section of our website](https://torproject.org/about/reports) for previous year's reports!