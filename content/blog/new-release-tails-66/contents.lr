title: New Release: Tails 6.6
---
pub_date: 2024-08-13
---
author: tails
---
categories:

partners
releases
---
summary:

Tails 6.6 is out.
---
body:

## Changes and updates

  * Update _Tor Browser_ to [13.5.2](https://blog.torproject.org/new-release-tor-browser-1352).

  * Update _Thunderbird_ to [115.14.0](https://www.thunderbird.net/en-US/thunderbird/115.14.0/releasenotes/).

  * Update [many](https://salsa.debian.org/kernel-team/firmware-nonfree/-/blob/master/debian/changelog?ref_type=heads) firmware packages. This improves the support for newer hardware: graphics, Wi-Fi, and so on.

## Fixed problems

#### Persistent Storage

  * Increase the maximum waiting time to 4 minutes when unlocking the Persistent Storage before returning an error. ([#20475](https://gitlab.tails.boum.org/tails/tails/-/issues/20475))

  * Made the creation of the Persistent Storage more robust after starting a Tails USB stick for the first time. ([#20451](https://gitlab.tails.boum.org/tails/tails/-/issues/20451))

  * Prevent the Persistent Storage settings from freezing after opening a link to the documentation. ([#20438](https://gitlab.tails.boum.org/tails/tails/-/issues/20438))

  * Prevent Additional Software from crashing when installing virtual packages. ([#20477](https://gitlab.tails.boum.org/tails/tails/-/issues/20477))

#### Networking

  * Fix connecting to the Tor network using default bridges. ([#20467](https://gitlab.tails.boum.org/tails/tails/-/issues/20467))

  * Allow enabling multiple network interfaces again. ([#20128](https://gitlab.tails.boum.org/tails/tails/-/issues/20128))

#### Tails Cloner

  * Remove 30 seconds of waiting time when installing by cloning. ([#20131](https://gitlab.tails.boum.org/tails/tails/-/issues/20131))

For more details, read our
[changelog](https://gitlab.tails.boum.org/tails/tails/-/blob/master/debian/changelog).

## Get Tails 6.6

### To upgrade your Tails USB stick and keep your Persistent Storage

  * Automatic upgrades are available from Tails 6.0 or later to 6.6.

  * If you cannot do an automatic upgrade or if Tails fails to start after an automatic upgrade, please try to do a [manual upgrade](https://tails.net/doc/upgrade/#manual).

### To install Tails 6.6 on a new USB stick

Follow our installation instructions:

  * [Install from Windows](https://tails.net/install/windows/)

  * [Install from macOS](https://tails.net/install/mac/)

  * [Install from Linux](https://tails.net/install/linux/)

  * [Install from Debian or Ubuntu using the command line and GnuPG](https://tails.net/install/expert/)

The Persistent Storage on the USB stick will be lost if you install instead of
upgrading.

### To download only

If you don't need installation or upgrade instructions, you can download Tails
6.6 directly:

  * [For USB sticks (USB image)](https://tails.net/install/download/)

  * [For DVDs and virtual machines (ISO image)](https://tails.net/install/download-iso/)

## Support and feedback

For support and feedback, visit the [Support
section](https://tails.net/support/) on the Tails website.
