title: Meeting the demands of tomorrow's Internet
---
author: isabela
---
pub_date: 2024-12-19
---
categories:

fundraising
community
---
summary: Reflecting on 2024 and looking forward to 2025. 
---
body:

## 2024: Strength in Solidarity

2024 was a year of coming together, forming stronger bonds with like-minded folks, to build a strong foundation that will withstand the challenges to privacy, anonymity, and access to information in the coming years.

In that vein, the biggest news for the Tor Project in 2024 was merging with Tails. Both organizations have been partners for many years, but [joining forces at an organizational level](https://blog.torproject.org/tor-tails-join-forces/) means that we can pool our resources and combine our strengths to work on the mission that we all share.

Together, Tor and Tails provide essential tools to help people around the world stay safe online. By merging as one organization we can more easily provide a suite of solutions that allow everyone to exercise their right to:

- speak without uninvited listeners

- search without being followed

- browse without being watched

Beyond our merge with Tails, the Tor Project has experienced tremendous growth over the past year – we’re now nearly three times the size we were just a few years ago in terms of staff and budget. We’re incredibly fortunate to have welcomed so many talented individuals to our organization during this time. Brilliant, hard-working people have joined the Tor Project to advance complex censorship and privacy solutions that are steadily gaining maturity and are soon ready to be shared with the world.

But growth isn't just about the organization – it's about the community. Across the globe, people continue to contribute to Tor in countless ways, and we’ve introduced many new ways to get involved with Tor, from Privacy Resilience Grants for organizations to run Tor trainings to monthly relay operator meetings. What unites us is the understanding that our mission has never been more urgent.

## 2025: Solidarity in Action

Capitalist and technology-enabled surveillance has [moved beyond targeting users with ads to targeting their lives.](https://blog.torproject.org/surveillance-as-a-service-global-impact-of-israeli-defense-technologies-on-privacy-human-rights/) This is why privacy online today means freedom tomorrow. Protecting our privacy secures our fundamental rights for the future.

I will be honest, it can be overwhelming; however, in times like this, I like to focus on what can be done instead of worrying about what hasn't happened yet. The most important thing is to act, no matter how difficult it can be during times of fear and stress. Pushing for incremental change and improvements requires small actions every day. We have to engage the folks that are willing to join our fight, pave the way for those actions, and build the communities we want collectively.

There is a lesson to be learned from merging with Tails in 2024 and our growth in the last several years: together we are stronger. And in 2025, I want to use this lesson as a guiding principle, that solidarity and collaboration are our greatest strengths. That means next year:

- We’ll work with the Tor community to come together with relay operator meetups, localization meetups, trainers meetups.

- We’ll work with our partners to help them provide digital security trainings all around the world.

- We’ll work with the internet freedom community to strengthen our tools and strategies.

- We’ll work to defend human rights by supporting coalitions and their work.

- And we’ll keep pushing to make Tor easier to integrate into other apps, so that the ecosystem of privacy-preserving tech can thrive together.

## Building blocks of a community-minded Internet

This year has been a milestone year for [Arti](https://gitlab.torproject.org/tpo/core/arti), our multi-year effort to rewrite Tor's codebase in a modern programming language called Rust. This new approach makes Tor safer, easier to use, integrate, and simpler to maintain. We've made [great progress towards eventually matching the capabilities of Tor's existing implementation (known as C-Tor)](https://youtu.be/HjPdReNmf_g?t=1051), and we're on track to make even more strides in 2025 as we move closer to completing Arti. 

Arti has been built from the beginning to make it easier to embed Tor by providing a flexible API. Many people and projects have asked for easier ways to integrate Tor capabilities into their applications, and [Arti gives them that power](https://blog.torproject.org/arti_1_2_8_released/). This will result in a wider ecosystem of tools that offer Tor protections to users who need it to circumvent censorship safely. 

Our vision is that Arti, built from the ground up to be easy to embed, will foster a more diverse ecosystem of apps and services that use Tor. As the mission to provide safe, private access to the open internet becomes more urgent, we envision a future where Tor is more widely available in more tools, and we believe that Arti makes that possible.

So, my message this year is: you can count on Tor. We will strive to make sure that our technology is available, usable and safe. We will continue to support users all around the world and continue to spread knowledge by teaching others on how to train people on Tor and other digital security tools. We will be aligned with those who are working to defend human rights and digital rights all around the world. You can count on us. And we know we can count on you too.

Thank you for all your support. We’ll be by your side on the front lines in 2025.

