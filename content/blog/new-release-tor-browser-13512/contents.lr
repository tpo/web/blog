title: New Release: Tor Browser 13.5.12
---
pub_date: 2025-02-04
---
_discoverable: no
---
author: pierov
---
categories:

applications
releases
---
summary: Tor Browser 13.5.12 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.5.12 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.5.12/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

Ths release is part of the legacy channel, meant to extend the support for Windows 7/8/8.1 and macOS 10.12-10.14.
If your OS is not one of these, you should download the latest stable from the 14.0 series instead.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The [full changelog](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.5/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) since Tor Browser 13.5.11 is:

- All Platforms
  - Updated Firefox to 115.20.0esr
  - [Bug tor-browser#43447](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43447): Backport stable and legacy: Some .tor.onion sites are not displaying the underlying V3 onion address in alpha
  - [Bug tor-browser#43450](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43450): Rebase Tor Browser legacy onto 115.20.0esr
  - [Bug tor-browser#43451](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43451): Backport security fixes from Firefox 135
- Build System
  - All Platforms
    - [Bug tor-browser-build#41324](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41324): Improve build signing ergonomics
    - [Bug tor-browser-build#41350](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41350): Increase timeout in rcodesign-notary-submit
    - [Bug tor-browser-build#41357](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41357): keyring/anti-censorship.gpg is in `GPG keybox database version 1` format
