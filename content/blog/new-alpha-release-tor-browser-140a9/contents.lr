title: New Alpha Release: Tor Browser 14.0a9
---
pub_date: 2024-10-09
---
author: morgan
---
categories:

applications
releases
---
summary: Tor Browser 14.0a9 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 14.0a9 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/14.0a9/).

This version includes important security updates to Firefox:
- https://www.mozilla.org/en-US/security/advisories/mfsa2024-51/#CVE-2024-9680

Users should update immediately.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 14.0a8](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - [Bug tor-browser#43197](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43197): Disable automatic exception for HTTPS-First
  - [Bug tor-browser#43201](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43201): Security fixes from Firefox 131.0.2
- Android
  - [Bug tor-browser#43132](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43132): Enable extensions installation on Tor Browser 14 for Android.
