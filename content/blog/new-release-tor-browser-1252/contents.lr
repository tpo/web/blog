title: New Release: Tor Browser 12.5.2
---
author: richard
---
pub_date: 2023-08-02
---
categories:
applications
releases
---
summary: Tor Browser 12.5.2 is now available from the Tor Browser download page and also from our distribution directory.
---
body:

Tor Browser 12.5.2 is now available from the Tor Browser [download page](https://www.torproject.org/download/) and also
from our [distribution directory](https://dist.torproject.org/torbrowser/12.5.2/).

This release updates Firefox to 102.14.0esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-30/).  We also backported the Android-specific [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-29/) from Firefox 116.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 12.5.1](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-12.5/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated Translations
  - Updated NoScript to 11.4.26
  - [Bug tor-browser#41908](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41908): Rebase stable 12.5 to 102.14esr
- Windows + macOS + Linux
  - Updated Firefox to 102.14.0esr
- Windows
  - [Bug tor-browser#41761](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41761): xul.dll win crash tor-browser 12.5.1 (based on Mozilla Firefox 102.13.0esr) (64-Bit)
- Android
  - Updated GeckoView to 102.14.0esr
  - [Bug tor-browser#41928](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41928): Backport Android-specific security fixes from Firefox 116 to ESR 102.14 / 115.1 - based Tor Browser
- Build System
  - All Platforms
    - Updated Go to 1.20.6
    - [Bug tor-browser-build#40889](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40889): Add mullvad sha256sums URL to tools/signing/download-unsigned-sha256sums-gpg-signatures-from-people-tpo
    - [Bug tor-browser-build#40894](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40894): Fix format of keyring/boklm.gpg
    - [Bug tor-browser-build#40909](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40909): Add dan_b and ma1 to list of taggers in relevant projects
  - Windows
    - [Bug tor-browser-build#31546](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/31546): Create and expose PDB files for Tor Browser debugging on Windows

