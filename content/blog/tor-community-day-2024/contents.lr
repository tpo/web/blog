title: Join us for the Tor Community Day 2024 in Lisbon
---
author: pavel
---
pub_date: 2024-05-15
---
categories:
community
human rights

---
summary: 
The event will take place in Lisbon, Portugal on May 25th, 2024. It is free and open to the public with sessions in English and Portuguese. 
---
body:

The Tor Project is happy to announce that we are hosting the Tor Community Day 2024 on May 25th in Lisbon, Portugal! The Community Day is a great opportunity for Tor Core Contributors, Tor Community Members, and other interested folks to come together, connect and discuss all things Tor. 

If you want to learn more about how Tor works, its impact in Portugal, how you can become a contributor–or simply want to discuss various topics related to free and open-source software, censorship circumvention, privacy and more–this event is for you!

### When
- Date: May 25th, 2024
- Time: 9:00 AM to 5:00 PM. (There will be an hour and a half break for lunch.)

### Where
- Location: Universidade NOVA de Lisboa, Colégio Almada Negreiros, Room 209.

### Who
- Organizers: The Tor Project and [PrivacyLx](https://privacylx.org)
- The Tor Community Day is free and open to the public. Anyone who wishes to join is welcome without any prior need to sign up. If you're curious about Tor, stop by!

### Program
- This is a bilingual event: The first half of the event will be in English, the second half in Portuguese.
- We'll have talks from the Tor Project and from members of the community in Portugal. 
- Be on the lookout for updates about the schedule and participating projects on our social channels and the Tor forum.

We are excited to meet you later this month!
  
  _-The Tor Project_

----

# Junta-te a nós para o Dia da Comunidade do Tor de 2024 em Lisboa

O Projeto Tor tem o prazer de anunciar que vamos organizar o Dia da Comunidade do Tor de 2024 no dia 25 de maio em Lisboa, Portugal! O Dia da Comunidade é uma grande oportunidade para os principais colaboradores do Tor, membros da comunidade do Tor e outras pessoas interessadas se reunirem, se conectarem e discutirem todas as coisas relacionadas com o Tor. 

Se queres saber mais sobre como o Tor funciona, o seu impacto em Portugal, como te podes tornar um contribuidor - ou simplesmente queres discutir vários tópicos relacionados com software livre e de código aberto, evasão à censura, privacidade e muito mais - este evento é para ti!

### Quando
- Data: 25 de maio de 2024
- Horário: 9h às 17h. (Haverá um intervalo de uma hora e meia para o almoço).


### Onde
- Localização: Universidade NOVA de Lisboa, Colégio Almada Negreiros, Sala 209.


### Quem
- Organizadores: The Tor Project e [PrivacyLx](https://privacylx.org)
- O Dia da Comunidade do Tor é gratuito e aberto ao público. Qualquer pessoa que deseje participar é bem-vinda sem necessidade de se inscrever previamente. Se estiveres curioso sobre o Tor, aparece!


### Programa
- Este é um evento bilingue: A primeira metade do evento será em inglês, a segunda metade em português.
- Teremos palestras do Projeto Tor e de membros da comunidade em Portugal. 
- Estejam atentos a actualizações sobre a programação e os projectos participantes nos nossos canais sociais e no fórum Tor.

Estamos ansiosos por vos encontrar no final deste mês!
  
  _-The Tor Project_
