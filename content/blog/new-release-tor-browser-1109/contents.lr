title: New Release: Tor Browser 11.0.9 (Windows, macOS, Linux)
---
pub_date: 2022-03-18
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.0.9 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.0.9 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.0.9/).

This releases fixes [bug tor-browser#40802](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40802) which caused some users to be unable to access client authorized onion services.

We use the opportunity as well to update various other components of Tor Browser:
- OpenSSL 1.1.1n

We also switch to the latest Go version (1.17.8) for building our Go-related projects.

The full changelog since [Tor Browser 11.0.7](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.0) is:

- Windows + OS X + Linux
  - [Bug tor-browser#40802](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40802): Client Auth dialog is broken in Tor Browser 11.0.6, works in 11.0.4
  - [Bug tor-browser#40831](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40831): Upgrade to OpenSSL 1.1.1n
  - [Bug tor-browser#40830](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40830): cherry-pick fix for bugzilla 1758156
 - Build System
  - Windows + OS X + Linux
    - Update Go to 1.17.8
    - [Bug tor-browser-build#40422](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40422): Remove projects/ed25519
    - [Bug tor-browser-build#40431](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40431): added license info for edwards25519 and edwards25519-extra
    - [Bug tor-browser-build#40436](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40436): Use intermediate files for default bridge lines
    - [Bug tor-browser-build#40438](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40438): Use the same list of bridges for desktop and android