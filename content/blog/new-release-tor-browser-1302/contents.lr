title: New Release: Tor Browser 13.0.2 (Android)
---
pub_date: 2023-10-27
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0.2 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0.2 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0.2/).

This release is identical to our 13.0.1 release, but fixes an issue with the Android apk version-code which collided with our 13.0 releases. This colliding version code prevented us from publishing to Google Play, so we have built 13.0.2 with an empty commit in order to generate a new non-colliding version code.

A long-term fix to our build-system to handle this case is being tracked in [tor-browser-build#40992](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40992).

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.0.1](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.0/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- Android
  - Bump firefox-android commit to generate new version code to allow uploading to Google Play (see [tor-browser-build#40992](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40992))
