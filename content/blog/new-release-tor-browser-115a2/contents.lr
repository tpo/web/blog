title: New Alpha Release: Tor Browser 11.5a2 (Windows, macOS, Linux)
---
pub_date: 2022-01-26
---
author: boklm
---
categories:

applications
releases
---
summary: Tor Browser 11.5a2 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5a2 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5a2/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-02/) to Firefox.

Tor Browser 11.5a2 updates Firefox to 91.5.0esr and includes bugfixes and
stability improvements. Additionally, the browser landing page gets the usual
Tor Browser look and feel back, removing the parts of our year end donation
campaign.

We use the opportunity as well to update various other components of Tor
Browser: Tor to 0.4.7.3-alpha, OpenSSL to 1.1.1m, and NoScript to 11.2.14. We
switch to the latest Go version (1.17.5), too, for building our Go-related
projects.

As usual, please report any issues you find in this alpha version, so we can
fix them for the next upcoming major stable release (11.5).

The full changelog since [Tor Browser 11.5a1](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master) is:

- Windows + OS X + Linux
  - Update Firefox to 91.5.0esr
  - Update Tor to 0.4.7.3-alpha
  - Update OpenSSL to 1.1.1m
  - Update NoScript to 11.2.14
  - [Bug tor-browser-build#40405](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40405): Rotate deusexmachina IP address
  - [Bug tor-browser#40645](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40645): Migrate Moat APIs to Moat.jsm module
  - [Bug tor-browser#40684](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40684): Misc UI bug fixes
  - [Bug tor-browser#40736](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40736): Disable third-party cookies in Private Browsing Mode
  - [Bug tor-browser#40756](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40756): Fix up wrong observer removals
  - [Bug torbutton#40758](https://gitlab.torproject.org/tpo/applications/torbutton/-/issues/40758): Remove YEC takeover from about:tor
  - Translations update
- Windows
  - [Bug tor-browser#40742](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40742): Remove workaround for fixing --disable-maintenance-service build bustage
  - [Bug tor-browser#40753](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40753): Revert fix for Youtube videos not playing on Windows
- Linux
  - [Bug tor-browser-build#40387](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40387): Fonts of the GUI do not render after update
  - [Bug tor-browser-build#40399](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40399): Bring back Noto Sans Gurmukhi and Sinhala fonts
  - [Bug tor-browser#40685](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40685): Monospace font in browser chrome
- Build System
  - Windows + OS X + Linux
    - [Bug tor-browser-build#40345](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40345): Update Go to 1.17.5
  - OS X
    - [Bug tor-browser-build#40390](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40390): Remove workaround for macOS OpenSSL build breakage
