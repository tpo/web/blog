title: New Alpha Release: Tor Browser 13.5a1
---
pub_date: 2023-11-02
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.5a1 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.5a1 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.5a1/).

This release updates Firefox to 115.4.0esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-46/). We also backported the Android-specific [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-45/) from Firefox 119.

The full changelog since [Tor Browser 13.0a6](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated OpenSSL to 3.0.12
  - Updated NoScript to 11.4.28
  - [Bug tor-browser#42191](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42191): Backport security fixes (Android &amp; wontfix) from Firefox 119 to 115.4 - based Tor Browser
  - [Bug tor-browser#42204](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42204): Drop unused aboutTor.dtd
  - [Bug tor-browser-build#40975](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40975): libstdc++.so.6 is included twice in tor-browser
- Windows + macOS + Linux
  - Updated Firefox to 115.4.0esr
  - [Bug tor-browser#41341](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41341): Fix style and position of "Always Prioritize Onions" wingpanel
  - [Bug tor-browser#42108](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42108): Tor Circuit button not shown if TLS handshake fails
  - [Bug tor-browser#42182](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42182): Default Search Engine Does Not Persist Through Shift to New Identity
  - [Bug tor-browser#42184](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42184): Setting "Homepage and new windows" ignores "Blank Page" value
- Windows + macOS
  - [Bug tor-browser#42154](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42154): Empty the clipboard on browser shutdown only if content comes from private browsing windows
- Android
  - Updated GeckoView to 115.4.0esr
  - [Bug tor-browser#42201](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42201): Make the script sign all the channels for local builds
  - [Bug tor-browser#42222](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42222): Fix TorDomainIsolator initialization on Android
- Build System
  - All Platforms
    - Update Go to 1.21.3
    - [Bug tor-browser-build#40852](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40852): Reproducible build of the the lox client library to wasm
    - [Bug tor-browser-build#40934](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40934): Remove $bundle_locales from signing scripts now that we're on ALL for everything
    - [Bug tor-browser-build#40976](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40976): Update download-unsigned-sha256sums-gpg-signatures-from-people-tpo to fetch from tb-build-02 and tb-build-03
    - [Bug tor-browser-build#40982](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40982): Fix logging in tools/signing/do-all-signing
    - [Bug tor-browser-build#40983](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40983): Bump the various branches to 13.5 on main
    - [Bug tor-browser-build#40989](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40989): Add .nobackup files to reproducible and disposable directores
    - [Bug rbm#40062](https://gitlab.torproject.org/tpo/applications/rbm/-/issues/40062): Copy input directories to containers recursively
  - Windows
    - [Bug tor-browser-build#40984](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40984): The PDBs for .exe are not included
  - macOS
    - [Bug tor-browser-build#29815](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/29815): Sign our macOS bundles on Linux
  - Linux
    - [Bug tor-browser-build#40979](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40979): Add redirects from old Linux bundle filename to the new one
