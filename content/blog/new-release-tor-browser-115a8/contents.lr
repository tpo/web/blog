title: New Alpha Release: Tor Browser 11.5a8 (Windows/macOS/Linux)
---
pub_date: 2022-03-23
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5a8 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5a8 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5a8/).

This releases fixes [bug tor-browser#40802](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40802) which caused some users to be unable to access client authorized onion services.

We use the opportunity as well to update various other components of Tor Browser:
- OpenSSL 1.1.1n

The full changelog since [Tor Browser 11.5a6](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master) is:

- Windows + OS X + Linux
  - [Bug tor-browser#14939](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/14939): Support ipv6 addresses in Tor Circuit Display
  - [Bug tor-browser#40460](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40460): Upgrade to OpenSSL 1.1.1n
  - [Bug tor-browser#40802](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40802): Client Auth dialog is broken in Tor Browser 11.06, works in 11.04
  - Bug 40830: cherry-pick fix for bugzilla 1758156
