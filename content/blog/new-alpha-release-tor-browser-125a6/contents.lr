title: New Alpha Release: Tor Browser 12.5a6 (Android, Windows, macOS, Linux)
---
pub_date: 2023-05-24
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 12.5a6 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 12.5a6 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/12.5a6/).

This release updates Firefox 102.11.0esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-17/). There were no Android-specific security updates to backport from the Firefox 113 release.

## Build-Signing Infrastructure Updates

We are in the process of updating our build signing infrastructure, and unfortunately are unable to ship code-signed 12.5a6 installers for Windows systems currently. Therefore we will not be providing full Window installers for this release. However, automatic build-to-build upgrades from 12.5a4 and 12.5a5 should continue to work as expected.

## Full changelog

The full changelog since [Tor Browser 12.5a5](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs/ChangeLog.txt) is:

- All Platforms
  - Updated Translations
  - Updated Go to 11.9.9
  - [Bug tor-browser-build#40860](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40860): Improve the transition from the old fontconfig file to the new one
  - [Bug tor-browser#41728](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41728): Pin bridges.torproject.org domains to Let's Encrypt's root cert public key
  - [Bug tor-browser#41738](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41738): Replace the patch to disable live reload with its preference
  - [Bug tor-browser#41757](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41757): Rebase Tor Browser Alpha to 102.11.0esr
  - [Bug tor-browser#41763](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41763): TTP-02-003 WP1: Data URI allows JS execution despite safest security level (Low)
  - [Bug tor-browser#41764](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41764): TTP-02-004 OOS: No user-activation required to download files (Low)
  - [Bug tor-browser#41775](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41775): Avoid re-defining some macros in nsUpdateDriver.cpp
- Windows + macOS + Linux
  - Updated Firefox to 102.11esr
  - [Bug tor-browser#41607](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41607): Update "New Circuit" icon
  - [Bug tor-browser#41736](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41736): Customize the default CustomizableUI toolbar using CustomizableUI.jsm
  - [Bug tor-browser#41770](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41770): Keyboard navigation broken leaving the toolbar tor circuit button
  - [Bug tor-browser#41777](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41777): Internally shippped manual does not adapt to RTL languages (it always align to the left)
- Windows + Linux
  - [Bug tor-browser#41654](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41654): UpdateInfo jumped into Data
- Linux
  - [Bug tor-browser#41732](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41732): implement linux font whitelist as defense-in-depth
  - [Bug tor-browser#41776](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41776): System fonts are temporarily leaked on Linux after the browser is updated from 12.5a4 or earlier
- Android
  - Updated GeckoView to 102.11esr
- Build System
  - All Platforms
    - [Bug tor-browser-build#33953](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/33953): Provide a way for easily updating Go dependencies of projects
    - [Bug tor-browser-build#40673](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40673): Avoid building each go module separately
    - [Bug tor-browser-build#40818](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40818): Enable wasm target for rust compiler
    - [Bug tor-browser-build#40841](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40841): Adapt signing scripts to new signing machines
    - [Bug tor-browser-build#40849](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40849): Move Go dependencies to the projects dependent on them, not as a standalone projects
    - [Bug tor-browser-build#40856](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40856): Unblock nightly builds
  - Windows
    - [Bug tor-browser-build#40846](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40846): Temporarily disable Windows signing
