title: New Release: Tor Browser 13.5.1
---
pub_date: 2024-07-10
---
author: boklm
---
categories:

applications
releases
---
summary: Tor Browser 13.5.1 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.5.1 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.5.1/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2024-30/) to Firefox.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.5](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.5/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - [Bug tor-browser#42689](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42689): Rebase Tor Browser onto 115.13.0esr
  - [Bug tor-browser#42693](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42693): Backports security fixes from Firefox 128
- Windows + macOS + Linux
  - Updated Firefox to 115.13.0esr
- Android
  - Updated GeckoView to 115.13.0esr
- Build System
  - All Platforms
    - Updated Go to 1.21.12
    - [Bug tor-browser-build#41166](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41166): Use the GitHub repository for firefox-l10n
  - Windows
    - [Bug tor-browser-build#41177](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41177): Include Windows installer without -portable- in download json files
