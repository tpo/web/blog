title: New Release: Tor Browser 11.0.2
---
pub_date: 2021-12-08
---
author: sysrqb
---
categories: applications
---
summary: Tor Browser 11.0.2 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.0.2 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.0.2/)

This version updates Firefox on Windows, macOS, and Linux to 91.4.0esr. This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2021-53/) to Firefox.

## Full changelog

The full changelog since [Tor Browser 11.0.1](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.0) is:

- Windows, MacOS &amp; Linux:
  - Update Firefox to 91.4.0esr
  - [Bug 40318](https://bugs.torproject.org/tpo/applications/tor-browser-build/40318): Remove check for DISPLAY env var in start-tor-browser
  - [Bug 40386](https://bugs.torproject.org/tpo/applications/tor-browser-build/40386): Add new default obfs4 bridge "deusexmachina"
  - [Bug 40682](https://bugs.torproject.org/tpo/applications/tor-browser/40682): Disable network.proxy.allow_bypass
- Linux
  - [Bug 40387](https://bugs.torproject.org/tpo/applications/tor-browser-build/40387): Remove some fonts on Linux

## Known issues

Tor Browser 11.0.2 comes with a number of known issues (please check the following list before submitting a new bug report):

- [Bug 40668](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40668): DocumentFreezer &amp; file scheme
- [Bug 40382](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40382): Fonts don’t render
- [Bug 40679](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40679): Missing features on first-time launch in esr91 on MacOS
- [Bug 40667](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40667): AV1 videos shows as corrupt files in Windows 8.1
- [Bug 40666](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40666): Switching svg.disable affects NoScript settings
- [Bug 40693](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40693): Potential Wayland dependency
- [Bug 40705](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40705): “visit our website” link on about:tbupdate pointing to different locations
- [Bug 40706](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40706): Fix issue in https-e wasm

