#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import click
import datetime
import json
import os
import re

from subprocess import run
from shutil import copytree
from lektor.metaformat import tokenize

ALT_SIDEBAR_PLACEHOLDER = "<!-- ##SIDEBAR## -->"


class BatchArgParser(argparse.ArgumentParser):
    """argument parsing helper"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_argument(
            "-a",
            "--archive",
            type=lambda s: datetime.datetime.strptime(s, "%Y-%m-%d"),
            help="Archive build artifacts older than specified date",
        )
        self.add_argument(
            "-o",
            "--output-path",
            action="store",
            required=True,
            help="Location of build artifacts (required)",
        )
        self.add_argument(
            "-p",
            "--post-process",
            action="store_true",
            help="Post-process build artifacts for publishing",
        )

    def parse_args(self, *args, **kwargs):
        args = super().parse_args(*args, **kwargs)
        return args


class BlogTools:
    """Additional blog processing that doesn't belong in the plugin"""

    def archive_content(self, outputpath, cutoff):
        """Archive prebuilt blog posts and event pages into 'archived' directory"""

        found_to_archive = False

        if not os.path.exists("archived"):
            os.mkdir("archived")

        # load archives databag
        with open('databags/archived_events.json', 'r') as f:
            events_json = json.load(f)
        with open('databags/archived_posts.json', 'r') as f:
            posts_json = json.load(f)

        for base in ["content/blog", "content/event"]:
            for root, _, files in os.walk(base):
                if re.match(base + "$", root):
                    continue
                if "contents.lr" in files:
                    contentsfile = os.path.join(root, "contents.lr")
                    item = {}
                    with open(contentsfile, "rb") as contents:
                        for key, lines in tokenize(contents, encoding="utf-8"):
                            if key in [
                                "author",
                                "pub_date",
                                "start_date",
                                "end_date",
                                "title",
                            ]:
                                item[key] = lines[0]
                            elif key == "categories":
                                item[key] = list(map(str.rstrip, lines))
                        if ("pub_date" in item.keys() and datetime.datetime.strptime(item["pub_date"], "%Y-%m-%d") < cutoff):
                            buildpath = os.path.basename(root)
                            self.archive_item(outputpath, buildpath, item, root)
                            posts_json[buildpath] = item
                            found_to_archive = True
                        elif ("start_date" in item.keys() and datetime.datetime.strptime(item["start_date"], "%Y-%m-%d") < cutoff):
                            buildpath = os.path.join("event", os.path.basename(root))
                            self.archive_item(outputpath, buildpath, item, root)
                            events_json[os.path.basename(root)] = item
                            found_to_archive = True

        if found_to_archive:
            # dump updated archives databag
            with open("databags/archived_events.json", "w") as f:
                json.dump(events_json, f, indent=2)
            with open("databags/archived_posts.json", "w") as f:
                json.dump(posts_json, f, indent=2)
        else:
            print(
                "No posts or events found to archive matching the date range specified."
            )

    def archive_item(self, outputpath, buildpath, contents, contentpath):
        """Archive one blog post or event"""

        # log message
        sign = click.style("A", fg="green")
        print(f"{sign} {buildpath}")

        # deduplicate lead image and attachments before archiving
        blog_images = os.path.join(outputpath, "static", "images", "blog")
        inline_images = os.path.join(
            outputpath, "static", "images", "blog", "inline-images"
        )
        jdupes_args = [
            "--quiet",
            "--linksoft",
            "-X",
            "onlyext:jpg,jpeg,png",
            "-O",
            blog_images,
            inline_images,
            "-R",
            os.path.join(outputpath, buildpath),
        ]
        run(["/usr/bin/jdupes"] + jdupes_args, check=True)

        # copy the build artifacts to the repository, preserving symlinks
        copytree(
            os.path.join(outputpath, buildpath),
            os.path.join("archived", buildpath),
            symlinks=True,
        )

        # replace variable sidebar contents with placeholder
        with open(os.path.join("archived", buildpath, "index.html"), "r") as index_file:
            html = index_file.read()
            archived_html = re.sub(
                r"(\s*<aside[^>]*>).*(\s*</aside>)",
                r"\1\n<!-- ##SIDEBAR## -->\n\2",
                html,
                flags=re.DOTALL,
            )
        with open(os.path.join("archived", buildpath, "index.html"), "w") as index_file:
            index_file.write(archived_html)

        # delete content files but keep directory
        # this will help content authors avoid url collisions
        for f in os.listdir(contentpath):
            os.remove(os.path.join(contentpath, f))
        open(os.path.join(contentpath, ".gitkeep"), "a").close()

    def post_process(self, outputpath):
        """Execute post-processing tasks"""

        self.post_process_archived("archived", outputpath)

    def post_process_archived(self, archivepath, outputpath):
        """Copy archived build artifacts to output directory"""

        # copy archived build artifacts to output path
        if os.path.exists(archivepath):
            copytree(archivepath, outputpath, symlinks=True, dirs_exist_ok=True)
        else:
            return

        # read sidebar html
        with open(os.path.join(outputpath, "sidebar/index.html"), "r") as sidebar_file:
            sidebar = sidebar_file.read()

        # swap sidebar placeholder in archived html
        for root, _, files in os.walk(archivepath):
            if "index.html" in files:
                buildpath = root.split(os.path.sep, 1)[1]
                htmlpath = os.path.join(outputpath, buildpath, "index.html")
                with open(htmlpath, "r") as index_file:
                    html = index_file.read()
                    # fix relative links
                    components = buildpath.split("/")
                    num_comp = len(components)
                    if num_comp > 1:
                        dots = "../" * num_comp
                        sb = re.sub(r'\bhref="../', 'href="' + dots, sidebar)
                    else:
                        sb = sidebar
                    # perform substitution
                    (html, subs) = re.subn(ALT_SIDEBAR_PLACEHOLDER, sb, html, count=1)
                if subs:
                    with open(htmlpath, "w") as index_file:
                        index_file.write(html)


def main():
    args = BatchArgParser().parse_args()
    blog_tools = BlogTools()

    if args.archive:
        blog_tools.archive_content(args.output_path, args.archive)
    elif args.post_process:
        blog_tools.post_process(args.output_path)


if __name__ == "__main__":
    main()
