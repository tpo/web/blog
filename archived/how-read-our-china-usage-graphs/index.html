<!doctype html>
<html>
<head>
    <title>How to read our China usage graphs | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="How to read our China usage graphs | Tor Project">
    <meta property="og:description" content="Recently somebody asked me why our usage numbers in China are so low. More precisely, his...">
    <meta property="og:image" content="https://blog.torproject.org/static/images/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/how-read-our-china-usage-graphs/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        How to read our China usage graphs
      </h1>
    <p class="meta">by arma | March 17, 2014</p>
    <picture>
      <source media="(min-width:415px)" srcset="../static/images/lead.webp" type="image/webp">
<source srcset="../static/images/lead_small.webp" type="image/webp">

      <img class="lead" src="../static/images/lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>Recently somebody asked me why our usage numbers in China are so low. More precisely, his question was "How do I read <a href="https://metrics.torproject.org/users.html?graph=userstats-bridge-country&amp;start=2011-10-18&amp;end=2014-01-16&amp;country=cn#userstats-bridge-country" rel="nofollow">this graph</a> in any way other than 'Tor is effectively blocked in China'?" After writing up an answer for him, I realized I should share it with the rest of the Tor community too.</p>

<p>The correct interpretation of the graph is "obfs3 bridges have not been deployed enough to keep up with the demand in China". So it isn't that Tor is blocked — it's that we haven't done much of a deployment for obfs3 bridges or ScrambleSuit bridges, which are the latest steps in the arms race.</p>

<p>The short explanation is that <a href="https://svn.torproject.org/svn/projects/design-paper/blocking.html#sec:network-fingerprint" rel="nofollow">the old vanilla SSL Tor transport</a> doesn't work in China anymore due to their <a href="http://freehaven.net/anonbib/#foci12-winter" rel="nofollow"> active probing infrastructure</a>.  The <a href="https://gitweb.torproject.org/pluggable-transports/obfsproxy.git/blob/HEAD:/doc/obfs2/obfs2-protocol-spec.txt" rel="nofollow">obfs2 transport</a> doesn't work anymore either, for the same reason. The <a href="https://gitweb.torproject.org/pluggable-transports/obfsproxy.git/blob/HEAD:/doc/obfs3/obfs3-protocol-spec.txt" rel="nofollow">obfs3 transport</a> works great for now, and thousands of people are happily using it — and some of those people aren't reflected in the graphs you see (I'll explain that more below).</p>

<p>The medium-length explanation is that we've been leading and coordinating the international research effort at understanding how to design and analyze <a href="https://www.torproject.org/docs/pluggable-transports" rel="nofollow">transports that resist both DPI and active probing</a>, and approximately none of these approaches have been deployed at a large scale yet. So it doesn't make sense to say that Tor is blocked in China, because it mischaracterizes Tor as a static protocol. "Tor" when it comes to censorship circumvention is a toolbox of options — some of them work in China, some don't. The ones that work (i.e. that should resist both DPI and active probing) haven't been rolled out very widely, in large part because we have funders who care about the research side but we have nobody who funds the operations, deployment, or scale-up side.</p>

<p>The long explanation is that it comes down to three issues:</p>

<p>First, there are some technical steps we haven't finished deploying in terms of collecting statistics about users of <a href="https://www.torproject.org/docs/bridges" rel="nofollow">bridges</a> + <a href="https://www.torproject.org/docs/pluggable-transports" rel="nofollow">pluggable transports</a>. The reason is that the server side of the pluggable transport needs to inform the Tor bridge what country the user was from, so the Tor bridge can include that in its (aggregated, anonymized) <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/196-transport-control-ports.txt" rel="nofollow">stats that it publishes to the metrics portal</a>.  We've now built most of the pieces, but most of the deployed bridges aren't running the new code yet. So the older bridges that are reporting their user statistics aren't seeing very many users from China, while the bridges that *aren't* reporting their user statistics, which are the ones that offer the newer pluggable transports, aren't well-represented in the graph. We have some nice volunteers looking into <a href="https://trac.torproject.org/projects/tor/ticket/10680" rel="nofollow">what fraction of deployed obfs3 bridges don't have this new 'extended ORPort' feature</a>. But (and you might notice the trend here) we don't have any funders currently who care about counting bridge users in China.</p>

<p>Second, we need to get more addresses. One approach is to get them from volunteers who sign up their computer as a bridge. That provides great sustainability in terms of community involvement (we did a similar push for obfs2 bridges back when Iran was messing with SSL, and got enough to make a real difference at the time), but one address per volunteer doesn't scale very well. The intuition is that the primary resource that relays volunteer is bandwidth, whereas the primary resource that bridges volunteer is their address — and while bandwidth is an ongoing contribution, once your IP address gets blocked then your contribution has ended, at least for the country that blocked it, or until you get another address via DHCP, etc. The more scalable approaches to getting bridge addresses involve coordinating with ISPs and network operators, and/or designs like <a href="https://crypto.stanford.edu/flashproxy/" rel="nofollow">Flashproxy</a> to make it really easy for users to sign up their address. I describe these ideas more in "approach four" and "approach five" of the <a href="https://blog.torproject.org/blog/strategies-getting-more-bridge-addresses" rel="nofollow">Strategies for getting more bridge addresses</a> blog post. But broad deployment of those approaches is again an operational thing, and we don't have any funded projects currently for doing it.</p>

<p>Third, we need ways of letting people learn about bridges and use them without getting them noticed. We used to think the arms race here was "how do you give out addresses such that the good guys can learn a few while the bad guys can't learn all of them", a la the bridges.torproject.org question. But it's increasingly clear that scanning resistance will be the name of the game in China: your transport has to not only blend in with many other flows (to drive up the number of scans they have to launch), but also when they connect to that endpoint and speak your protocol, your service needs to look unobjectionable there as well.  Some combination of <a href="http://www.cs.kau.se/philwint/scramblesuit/" rel="nofollow">ScrambleSuit</a> and <a href="https://fteproxy.org/" rel="nofollow">FTE</a> are great starts here, but it sure is a good thing that the research world has been working on so many prototype designs lately.</p>

<p>So where does that leave us? It would be neat to think about a broad deployment and operations plan here. I would want to do it in conjunction with some other groups, like Team Cymru on the technical platform side and some on-the-ground partner groups for distributing bridge addresses more effectively among social networks. We've made some good progress on the underlying technologies that would increase the success chances of such a deployment — though we've mostly been doing it using volunteers in our spare time on the side, so it's slower going than it could be. And several other groups (e.g. <a href="https://blog.torservers.net/20131213/torservers-awarded-250000-by-digital-defenders.html" rel="nofollow">torservers.net</a>) have recently gotten funding for deploying Tor bridges, so maybe we could combine well with them.</p>

<p>In any case it won't be a quick and simple job, since all these pieces have to come together. It's increasingly clear that just getting addresses should be the easy part of this. It's how you give them out, and what you run on the server side to defeat China's scanning, that still look like the two tough challenges for somebody trying to scale up their circumvention tool design.</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/circumvention">
          circumvention
        </a>
      </li><li>
        <a href="../category/global-south">
          global south
        </a>
      </li><li>
        <a href="../category/research">
          research
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-54789"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-54789" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 17, 2014</p>
    </div>
    <a href="#comment-54789">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-54789" class="permalink" rel="bookmark">I&#039;m looking forward for the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm looking forward for the day where all Tor relays are obfs3 by default. Isn't it a mere software issue,, or am I missing something?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-54812"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-54812" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">March 17, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-54789" class="permalink" rel="bookmark">I&#039;m looking forward for the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-54812">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-54812" class="permalink" rel="bookmark">You mean adding a layer of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You mean adding a layer of obfs3 over the normal link encryption between and to relays? We could do that, and it wouldn't be that hard (a simple matter of programming, as they say). But the list of relays is public, and it's not easy/simple to keep it secret: <a href="https://www.torproject.org/docs/faq#HideExits" rel="nofollow">https://www.torproject.org/docs/faq#HideExits</a></p>
<p>So it's not obvious what exactly this would buy us. Sounds like a great topic for a Tor dev proposal:<br />
<a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/001-process.txt" rel="nofollow">https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/001-proc…</a><br />
<a href="https://gitweb.torproject.org/torspec.git/tree/HEAD:/proposals" rel="nofollow">https://gitweb.torproject.org/torspec.git/tree/HEAD:/proposals</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-54796"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-54796" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 17, 2014</p>
    </div>
    <a href="#comment-54796">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-54796" class="permalink" rel="bookmark">Tech dummy but I have set up</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tech dummy but I have set up bridges by default inthe past.<br />
Recently around the time you report someone was able to block them and ask we run another 'thing" the usage on mine dropped to nil.</p>
<p>So my question is does it do much/any good to run the bridges by default?<br />
For the less tech savy?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-54813"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-54813" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">March 17, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-54796" class="permalink" rel="bookmark">Tech dummy but I have set up</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-54813">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-54813" class="permalink" rel="bookmark">Yes, it does good.
Our</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, it does good.</p>
<p>Our bridge distribution strategy involves breaking up the volunteer bridge addresses into different "buckets", where each bucket is given out in a different way. So some buckets will be given out quickly and easily, and probably blocked after a while. Whereas other buckets will be given out slowly and through hard-to-use mechanisms, so they'll have less load but also be less likely to be blocked.</p>
<p>Part of what we need to do a better job on is communicating to the bridge operator which bucket her bridge has been put in, so she can better understand her contribution.</p>
<p>But you're right, running a bridge on a single address is not as useful a contribution, long-term, as some of the more scalable approaches I describe in the post.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-54798"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-54798" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 17, 2014</p>
    </div>
    <a href="#comment-54798">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-54798" class="permalink" rel="bookmark">Thanks for your discussion.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for your discussion.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-54806"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-54806" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 17, 2014</p>
    </div>
    <a href="#comment-54806">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-54806" class="permalink" rel="bookmark">I wholeheartedly agree about</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I wholeheartedly agree about the importance of the Tor Project getting more funding for projects like this, but couldn't help but notice that if core devs--as opposed to people working primarily on advocacy--weren't talking about how they want to "burn it [the state] to the ground" on Twitter all day (<a href="https://twitter.com/puellavulnerata/status/445517416596922369" rel="nofollow">https://twitter.com/puellavulnerata/status/445517416596922369</a>) instead of writing code, there might be more person-hours available to the organization for projects like this at current resource levels. As someone volunteering patches and documentation for tor, sometimes that's hard not to notice.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-55397"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-55397" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 05, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-54806" class="permalink" rel="bookmark">I wholeheartedly agree about</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-55397">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-55397" class="permalink" rel="bookmark">Do you really think that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do you really think that tweet was time consuming to the point of taking away from Tor development, or were you merely pointing out something potentially controversial?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-54828"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-54828" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 18, 2014</p>
    </div>
    <a href="#comment-54828">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-54828" class="permalink" rel="bookmark">&quot;So it isn&#039;t that Tor is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"So it isn't that Tor is blocked — it's that we haven't done much of a deployment for obfs3 bridges or ScrambleSuit bridges, which are the latest steps in the arms race."</p>
<p>In fact, Tor is blocked in China. Tor is not usable for the people who want to circumvent the censorship. Psiphon, Lantern, and other VPNs just work. They do not require extra steps. You download, run it, access the blocked content. You can argue in hundreds of words about technical reasons, we don't care. Tor doesn't work without lots of extra steps and efforts.</p>
<p>Tor requires you download a huge file, run it, find it does not work, waste time looking around for solutions, eventually find bridges, find the bridges don't work, waste more time looking around for solutions, find obfs bridges, hopefully your bridge is online, then maybe you can get tor to work.</p>
<p>Tor in China needs to be: download, run, access the site. I want a tool that works, not an ideology packaged in a multi-step challenge. Xbox and PS4 are more fun at multi-step challenge than tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-55003"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-55003" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 25, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-54828" class="permalink" rel="bookmark">&quot;So it isn&#039;t that Tor is</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-55003">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-55003" class="permalink" rel="bookmark">That&#039;s a bit stupid for me</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's a bit stupid for me to say than "Tor in China needs to be: download, run, access the site. I want a tool that works, not an ideology packaged in a multi-step challenge. Xbox and PS4 are more fun at multi-step challenge than tor. ".</p>
<p>Tor isn't an ideology, it's a software. And it's 60% runned by the US, so i think than it's normal for them to try being able to adapt to every situations (US marines in Afghanistan used TOR to reach their online stuff).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-55020"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-55020" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">March 25, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-55003" class="permalink" rel="bookmark">That&#039;s a bit stupid for me</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-55020">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-55020" class="permalink" rel="bookmark">While we&#039;re picking on weird</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>While we're picking on weird phrases, what do you mean "60% runned by the US"? Almost all interpretations of that phrase are mistaken.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-55130"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-55130" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 30, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-54828" class="permalink" rel="bookmark">&quot;So it isn&#039;t that Tor is</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-55130">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-55130" class="permalink" rel="bookmark">Psiphon基本是连不上的</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Psiphon基本是连不上的。<br />
Lantern第1版死掉后，第2版也是极小范围在试用。<br />
其他VPN，很多是PPTP，鉴于pptp的弱点，就不多说为什么会让它存活了。</p>
<p>tor跟其他的VPN有个显著差别，VPN是为了能跨越gfw，而tor是在跨越的同时力求保证匿名。</p>
<p>为什么tor一直被追杀，原因就在于此。等到被认为不具备匿名作用了，arms race也就停止了。</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-55217"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-55217" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 02, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-55130" class="permalink" rel="bookmark">Psiphon基本是连不上的</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-55217">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-55217" class="permalink" rel="bookmark">Psiphon被封杀，但是可</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Psiphon被封杀，但是可以先开启海外的VPN，就能连接成功，等待几分钟就能更新服务器列表。。这样重复几次，就可以断开VPN，用Psiphon3直接连通了。</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-66312"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66312" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 26, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-54828" class="permalink" rel="bookmark">&quot;So it isn&#039;t that Tor is</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66312">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66312" class="permalink" rel="bookmark">I wholeheartedly agree. I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I wholeheartedly agree. I came here, because I was trying to find a way to set up Tor from within China, but instead of giving a detailed explanation of how to go about that, the Tor guys apparently are more interested in uptalking statistics. Who on earth cares about that???</p>
<p>I just wasted quite some time trying to connect a buddy of mine residing in China to the Tor-network with obfs3, but no cigar. The meek version didn't work either and there's no information of how to do it, only differening hints on other websites of which none actually worked. </p>
<p>To the blog author: Next time you feel you need to justify Tor's usability, please make sure that the system is actually easily and readily usable by the less tech-savvy masses. I don't doubt that there are thousands of Tor users in China that have a high enough pain threshold to actually figure out, how to get a crappy connection through Tor. Yet, that still is nothing compared to the 650 mio online users the country has right now...</p>
<p>The filesize of Tor might seem laughable compared to western standards, but try to download 30 megs with a connection that craps out constantly only to realize that you end up with an outdated version of the Firefox browser of which you already have a newer version installed and a rather tiny Tor loader. Why is there no plugin for FF instead?</p>
<p>Thanks to the comment writer for mentioning all those VPN-services. I have been trying out quite a few and none worked. So far I've been offering my line through Hamachi as a Proxy to my buddy, but I'm not online all day and my bandwidth can't match that of professional VPN services, but at least it works rekiably without problems.</p>
<p>I can't wait to give all these VPN-services a shot.</p>
<p>&gt; Tor in China needs to be: download, run, access the site.</p>
<p>Bingo! I'm stumped that the software doesn't even offer an automatic connection mode where it actually tries to connect and then goes through all possibilities automatically after a certain time out, instead of having the user to switch between protocols manually...</p>
<p>I know you guys hate Ultrasurf, but at least it couldn't be easier to use...</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-54865"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-54865" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 19, 2014</p>
    </div>
    <a href="#comment-54865">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-54865" class="permalink" rel="bookmark">&quot;Psiphon, Lantern, and other</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><cite>"Psiphon, Lantern, and other VPNs just work. They do not require extra steps."</cite></p>
<p>You forgot to mention the most popular one: Goagent. For normal users, it's more difficult to use than Tor. </p>
<p><cite>"You can argue in hundreds of words about technical reasons, we don't care."</cite></p>
<p>It's not 'about technical reasons'. It is about 'we've got a plan and Tor is not and will never be blocked in China, but we have no money' :P</p>
<p><cite>"Tor requires you download a huge file, run it, find it does not work, waste time looking around..., waste more time ...hopefully your bridge is online"</cite></p>
<p>I can feel your frustrated-with-Tor roar and sometimes I have the exact same feelings. But like you said, Tor is just one of the free services available online. If it works, great; if not, I'll still appreciate for the help and try other tools.</p>
<p>建议试下最新测试版 Tor Browser 3.6-beta-1，支持 obfs3，感觉设置方便有改进。（下载链接：<a href="https://www.torservers.net/mirrors/torproject.org/dist/torbrowser/" rel="nofollow">https://www.torservers.net/mirrors/torproject.org/dist/torbrowser/</a>）。另外，可以直接邮件问 <a href="mailto:help-zh@rt.torproject.org" rel="nofollow">help-zh@rt.torproject.org</a> 发一个可用obfs3网桥。</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-56428"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-56428" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 20, 2014</p>
    </div>
    <a href="#comment-56428">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-56428" class="permalink" rel="bookmark">Tor连接太慢</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor连接太慢</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-67432"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-67432" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 04, 2014</p>
    </div>
    <a href="#comment-67432">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-67432" class="permalink" rel="bookmark">Well, I&#039;m another fool who</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well, I'm another fool who attemped to use Tor in China. Well Tor is not blocked in China, but its not working.... As useful as a car with no wheels. What a joke...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-67993"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-67993" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 08, 2014</p>
    </div>
    <a href="#comment-67993">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-67993" class="permalink" rel="bookmark">What if there was some way</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What if there was some way to get an "allowed" list to people running outside bridges? Then it would be a matter of swapping addresses rather than just giving the address of the bridge. Getting onto a list could require some sort of "catchpa" style thing, which could prevent, or slow down the process of automated scanning.</p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
