<!doctype html>
<html>
<head>
    <title>Deterministic Builds Part Two: Technical Details | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Deterministic Builds Part Two: Technical Details | Tor Project">
    <meta property="og:description" content="This is the second post in a two-part series on the build security improvements in the Tor...">
    <meta property="og:image" content="https://blog.torproject.org/static/images/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/deterministic-builds-part-two-technical-details/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Deterministic Builds Part Two: Technical Details
      </h1>
    <p class="meta">by mikeperry | October 4, 2013</p>
    <picture>
      <source media="(min-width:415px)" srcset="../static/images/lead.webp" type="image/webp">
<source srcset="../static/images/lead_small.webp" type="image/webp">

      <img class="lead" src="../static/images/lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>This is the second post in a two-part series on the build security improvements in the <a href="https://blog.torproject.org/category/tags/tbb-30" rel="nofollow">Tor Browser Bundle 3.0 release cycle</a>.</p>

<p>The <a href="https://blog.torproject.org/blog/deterministic-builds-part-one-cyberwar-and-global-compromise" rel="nofollow">first post</a> described why such security is necessary. This post is meant to describe the technical details with respect to how such builds are produced.</p>

<p>We achieve our build security through a reproducible build process that enables anyone to produce byte-for-byte identical binaries to the ones we release. Elsewhere on the Internet, this process is varyingly called "deterministic builds", "reproducible builds", "idempotent builds", and probably a few other terms, too.</p>

<p>To produce byte-for-byte identical packages, we use <a href="http://gitian.org/" rel="nofollow">Gitian</a> to build Tor Browser Bundle 3.0 and above, but that isn't the only option for achieving reproducible builds. We will first describe how we use Gitian, and then go on to enumerate the individual issues that Gitian solves for us, and that we had to solve ourselves through either wrapper scripts, hacks, build process patches, and (in one esoteric case for Windows) direct binary patching.</p>

<h1>Gitian: What is it?</h1>

<p><a href="http://gitian.org/howto.html" rel="nofollow">Gitian</a> is a thin wrapper around the Ubuntu virtualization tools written in a combination of Ruby and bash. It was originally developed by Bitcoin developers to ensure the build security and integrity of the Bitcoin software.</p>

<p>Gitian uses Ubuntu's python-vmbuilder to create a qcow2 base image for an Ubuntu version and architecture combination and a set of git and tarball inputs that you specify in a '<a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/descriptors/linux/gitian-firefox.yml" rel="nofollow">descriptor</a>', and then proceeds to run a shell script that you provide to build a component inside that controlled environment. This build process produces an output set that includes the compiled result and another "output descriptor" that captures the versions and hashes of all packages present on the machine during compilation.</p>

<p>Gitian requires either Intel VT support (for qemu-kvm), or LXC support, and currently only supports launching Ubuntu build environments from Ubuntu itself.</p>

<h1>Gitian: How Tor Uses It</h1>

<p><a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/README.build" rel="nofollow">Tor's use of Gitian</a> is slightly more automated and also slightly different than how Bitcoin uses it.</p>

<p>First of all, because Gitian supports only Ubuntu hosts and targets, we must cross compile everything for Windows and MacOS. Luckily, Mozilla <a href="https://developer.mozilla.org/en-US/docs/Cross_Compile_Mozilla_for_Mingw32" rel="nofollow">provides support</a> for <a href="http://mingw-w64.sourceforge.net/" rel="nofollow">MinGW-w64</a> as a "third tier" compiler, and does endeavor to work with the MinGW team to fix issues as they arise.</p>

<p>To our further good fortune, we were able to use a MacOS cross compiler created by <a href="https://github.com/mingwandroid" rel="nofollow">Ray Donnelly</a> based on a <a href="https://github.com/mingwandroid/toolchain4" rel="nofollow">fork of "Toolchain4"</a>. We owe Ray a great deal for providing his compilers to the public, and he has been most excellent in terms of helping us through any issues we encountered with them. Ray is also working to <a href="https://github.com/diorcety/crosstool-ng.git" rel="nofollow">merge his patches</a> into the <a href="http://crosstool-ng.org/" rel="nofollow">crosstools-ng project</a>, to provide a more seamless build process to create rebuilds of his compilers. As of this writing, we are still using <a href="https://mingw-and-ndk.googlecode.com/files/multiarch-darwin11-cctools127.2-gcc42-5666.3-llvmgcc42-2336.1-Linux-120724.tar.xz" rel="nofollow">his binaries</a> in combination with the <a href="https://launchpad.net/~flosoft/+archive/cross-apple/+packages" rel="nofollow">flosoft MacOS10.X SDK</a>.</p>

<p>For each platform, we build the components of Tor Browser Bundle in 3 stages, with one descriptor per stage. The <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/descriptors/linux/gitian-tor.yml" rel="nofollow">first descriptor</a> builds Tor and its core dependency libraries (OpenSSL, libevent, and zlib) and produces one output zip file. The <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/descriptors/linux/gitian-firefox.yml" rel="nofollow">second descriptor</a> builds Firefox. The <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/descriptors/linux/gitian-bundle.yml" rel="nofollow">third descriptor</a> combines the previous two outputs along with our included Firefox addons and localization files to produce the actual localized bundle files.</p>

<p>We <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/tree/HEAD:/gitian/" rel="nofollow">provide a Makefile and shellscript-based wrapper</a> around Gitian to automate the download and authentication of our source inputs prior to build, and to perform a final step that creates a sha256sums.txt file that lists all of the bundle hashes, and can be signed by any number of detached signatures, one for each builder.</p>

<p>It is important to distribute multiple cryptographic signatures to prevent targeted attacks against stealing a single build signing key (because the signing key itself is of course another single point of failure). Unfortunately, GPG currently lacks  support for verifying multiple signatures of the same document. Users must manually copy each detached signature they wish to verify into its proper .asc filename suffix. Eventually, we hope to create a stub installer or wrapper script of some kind to simplify this step, as well as add multi-signature support to the Firefox update process. We are also investigating adding a URL and a hash of the package list the <a href="https://gitweb.torproject.org/torspec.git/blob/master:/dir-spec.txt" rel="nofollow">Tor Consensus</a>, so that the Tor Consensus document itself authenticates our binary packages.</p>

<p>We do not use the Gitian output descriptors or the Gitian signing tools, because the tools are used to sign Gitian's output descriptors. We found that Ubuntu's individual packages (which are listed in the output descriptors) varied too frequently to allow this mechanism to be reproducible for very long. However, we do include a <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/versions" rel="nofollow">list of input versions and hashes</a> used to produce each bundle in the bundle itself. The format of this versions file is the same that we use as input to download the sources. This means it should remain possible to re-build an arbitrary bundle for verification at a later date, assuming that any later updates to Ubuntu's toolchain packages do not change the output.</p>

<h1>Gitian: Pain Points</h1>

<p>Gitian is not perfect. In fact, many who have tried our build system have remarked that it is not even close to deterministic (and that for this and other reasons 'Reproducible Builds' is a better term). In fact, it seems to experience build failures for quite unpredictible reasons related to bugs in one or more of qemu-kvm/LXC, make, qcow copy-on-write image support. These bugs are often intermittent, and simply restarting the build process often causes things to proceed smoothly. This has made the bugs exceedingly tricky to pinpoint and diagnose.</p>

<p>Gitian's use of tags (especially signed tags) has some bugs and flaws. For this reason, we verify signatures ourselves after input fetching, and provide gitian only with explicit commit hashes for the input source repositories.</p>

<p>We maintain a list of the most common issues in the <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/README.build" rel="nofollow">build instructions</a>.</p>

<h1>Remaining Build Reproducibility Issues</h1>

<p>By default, the Gitian VM environment controls the following aspects of the build platform that normally vary and often leak into compiled software: hostname, build path, uname output, toolchain version, and time.</p>

<p>However, Gitian is not enough by itself to magically produce reproducible builds. Beyond what Gitian provides, we had to patch a number of reproducibility issues in Firefox and some of the supporting tools on each platform. These include:</p>

<ol>
<li><b>Reordering due to inode ordering differences (exposed via Python's os.walk())</b><br />
 Several places in the Firefox build process use python scripts to repackage both compiled library archives and zip files. In particular, they tend to obtain directory listings using os.walk(), which is dependent upon the inode ordering of the filesystem. We fix this by sorting those file lists in the applicable places.
 </li>
<li><b>LC_ALL localizations alter sorting order</b><br />
 Sorting only gets you so far, though, if someone from a different locale is trying to reproduce your build. Differences in your character sets will cause these sort orders to differ. For this reason, we set the LC_ALL environment variable to 'C' at the top of our Gitian descriptors.
 </li>
<li><b>Hostname and other OS info leaks in LXC mode</b><br />
 For these cases, we simply patch the pieces of Firefox that include the hostname (primarily for <a href="buildconfig" rel="nofollow">about:buildconfig</a>).
 </li>
<li><b>Millisecond and below timestamps are not fixed by libfaketime</b><br />
 Gitian relies on <a href="http://www.code-wizards.com/projects/libfaketime/" rel="nofollow">libfaketime</a> to set the clock to a fixed value to deal with embedded timestamps in archives and in the build process. However, in some places, Firefox inserts millisecond timestamps into its supporting libraries as part of an informational structure. We simply zero these fields.
 </li>
<li><b>FIPS-140 mode generates throwaway signing keys</b><br />
 A rather insane subsection of the <a href="https://en.wikipedia.org/wiki/FIPS_140" rel="nofollow">FIPS-140</a> certification standard requires that you distribute signatures for all of your cryptographic libraries. The Firefox build process meets this requirement by generating a temporary key, using it to sign the libraries, and discarding the private portion of that key. Because there are many other ways to intercept the crypto outside of modifying the actual DLL images, we opted to simply remove these signature files from distribution. There simply is no way to verify code integrity on a running system without both OS and coprocessor assistance. Download package signatures make sense of course, but we handle those another way (as mentioned above).
 </li>
<li><b>On Windows builds, something mysterious causes 3 bytes to randomly vary<br />
in the binary.</b><br />
 Unable to determine the source of this, we just bitstomp the binary and regenerate the PE header checksums using strip... Seems fine so far! ;)
 </li>
<li><b>umask leaks into LXC mode in some cases</b><br />
 We fix this by manually setting umask at the top of our Gitian descriptors. Additionally, we found that we had to reset the permissions inside of tar and zip files, as the umask didn't affect them on some builds (but not others...)
 </li>
<li><b>Zip and Tar reordering and attribute issues</b><br />
 To aid with this and other issues with reproducibility, we created simple shell wrappers for <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/build-helpers/dzip.sh" rel="nofollow">zip</a> and <a href="https://gitweb.torproject.org/builders/tor-browser-bundle.git/blob/HEAD:/gitian/build-helpers/dtar.sh" rel="nofollow">tar</a> to eliminate the sources of non-determinism.
 </li>
<li><b>Timezone leaks</b><br />
 To deal with these, we set TZ=UTC at the top of our descriptors.
</li>
</ol>

<h1>Future Work</h1>

<p>The most common question we've been asked about this build process is: What can be done to prevent the adversary from compromising the (substantially weaker) Ubuntu build and packaging processes, and further, what about the <a href="http://cm.bell-labs.com/who/ken/trust.html" rel="nofollow">Trusting Trust attack</a>?</p>

<p>In terms of eliminating the remaining single points of compromise, the first order of business is to build all of our compilers and toolchain directly from sources via their own Gitian descriptors.</p>

<p>Once this is accomplished, we can begin the process of building identical binaries from multiple different Linux distributions. This would require the adversary to compromise multiple Linux distributions in order to compromise the Tor software distribution.</p>

<p>If we can support reproducible builds through cross compiling from multiple architectures (Intel, ARM, MIPS, PowerPC, etc), this also reduces the likelihood of a Trusting Trust attack surviving unnoticed in the toolchain (because the machine code that injects the payload would have to be pre-compiled and present for all copies of the cross-compiled executable code in a way that is still not visible in the sources).</p>

<p>If those Linux distributions also support reproducible builds of the full build and toolchain environment (both <a href="https://wiki.debian.org/ReproducibleBuilds" rel="nofollow">Debian</a> and <a href="http://securityblog.redhat.com/2013/09/18/reproducible-builds-for-fedora/" rel="nofollow">Fedora</a> have started this), we can eliminate Trusting Trust attacks entirely by using <a href="http://www.schneier.com/blog/archives/2006/01/countering_trus.html" rel="nofollow">Diverse Double Compilation</a> between multiple independent distribution toolchains, and/or assembly audited compilers. In other words, we could use the distributions' deterministic build processes to <a href="https://lwn.net/Articles/555902/" rel="nofollow">verify</a> that identical build environments are produced through Diverse Double Compilation.</p>

<p>As can be seen, much work remains before the system is fully resistant against all forms of malware injection, but even in their current state, reproducible builds are a huge step forward in software build security. We hope this information helps other software distributors to follow the example set by Bitcoin and Tor.</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/releases">
          releases
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-35764"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-35764" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 04, 2013</p>
    </div>
    <a href="#comment-35764">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-35764" class="permalink" rel="bookmark">Thank you very much for your</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you very much for your hard work in this area.  I don't think most people realize just how difficult what you have accomplished is.</p>
<p>In particular, I chuckled upon reading about the 3 mysterious bytes for windows solution.</p>
<p>And I also wanted to mention that the diverse compiling on different architextures solution to trusting trust is quite brilliant, obvious, simple, and powerfull.</p>
<p>It would seem that the NSA next move in response would be persistant ultra low level rootkits, preferrable on every machine in the world.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-35850"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-35850" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 05, 2013</p>
    </div>
    <a href="#comment-35850">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-35850" class="permalink" rel="bookmark">Thanks for posting this I am</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for posting this I am using the David Wheeler method of deterministic builds for Android. CyanogenMod team should so the same</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-35890"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-35890" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 06, 2013</p>
    </div>
    <a href="#comment-35890">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-35890" class="permalink" rel="bookmark">Dead link :( --&gt; &quot;Trusting</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Dead link :( --&gt; "Trusting Trust attack" - <a href="http://cm.bell-labs.com/who/ken/trust.html" rel="nofollow">http://cm.bell-labs.com/who/ken/trust.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-36102"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36102" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 10, 2013</p>
    </div>
    <a href="#comment-36102">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36102" class="permalink" rel="bookmark">This all sounds like a great</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This all sounds like a great way to make an easy non moving target for the NSA to exploit the tor network.</p>
<p>Perhaps you should just use a build system burned onto a DVD and have it blank an SD card on boot and download the sources. Then use a second system to do the build that is not network connected.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-36153"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36153" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 11, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-36102" class="permalink" rel="bookmark">This all sounds like a great</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-36153">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36153" class="permalink" rel="bookmark">Did you read the first post</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Did you read the first post in this series at all? Have you heard of Stuxnet and Flame? They were specifically designed by the NSA to attack offline computers.</p>
<p>Any single build machine just makes for a juicy target to attack. Even a build machine with DVD media can have its BIOS or other firmwares infected.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-36156"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36156" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 12, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-36153" class="permalink" rel="bookmark">Did you read the first post</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-36156">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36156" class="permalink" rel="bookmark">all is fine YOU have crisis</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>all is fine YOU have crisis and THEY have toys</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-36336"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36336" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 17, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-36156" class="permalink" rel="bookmark">all is fine YOU have crisis</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-36336">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36336" class="permalink" rel="bookmark">Actually, the WORLD has a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Actually, the WORLD has a crisis because THEY have shown EVERYONE how to compromise global single points of failure by targeting specific machines.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-36170"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36170" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 12, 2013</p>
    </div>
    <a href="#comment-36170">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36170" class="permalink" rel="bookmark">I miss those little &quot;do not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I miss those little "do not write" tabs on VHS tapes and floppy disks. Those were the good old days.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-36947"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36947" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 06, 2013</p>
    </div>
    <a href="#comment-36947">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36947" class="permalink" rel="bookmark">FYI the PE header has a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>FYI the PE header has a timestamp field, which is probably what you're seeing on your Windows builds:<br />
<a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ms680313%28v=vs.85%29.aspx" rel="nofollow">http://msdn.microsoft.com/en-us/library/windows/desktop/ms680313%28v=vs…</a></p>
<p>DWORD TimeDateStamp;</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-37260"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-37260" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 09, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-36947" class="permalink" rel="bookmark">FYI the PE header has a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-37260">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-37260" class="permalink" rel="bookmark">Check</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Check out<br />
<a href="https://trac.torproject.org/projects/tor/ticket/10026" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/10026</a><br />
and<br />
<a href="https://trac.torproject.org/projects/tor/ticket/10102" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/10102</a></p>
<p>for the current theories (including apparently a fix).</p>
<p>I think your suggestion here is different from those?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-37135"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-37135" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 08, 2013</p>
    </div>
    <a href="#comment-37135">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-37135" class="permalink" rel="bookmark">Interesting project. Why</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Interesting project. Why consider writing a wrapper to extend gpg, instead of patching gpg to do what you want? Perhaps upstream would welcome the feature.</p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
