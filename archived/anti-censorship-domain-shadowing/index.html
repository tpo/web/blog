<!doctype html>
<html>
<head>
    <title>Domain Shadowing: Leveraging CDNs for Robust Blocking-Resistant Communications | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Domain Shadowing: Leveraging CDNs for Robust Blocking-Resistant Communications | Tor Project">
    <meta property="og:description" content="We invited guest blog author, Mingkui Wei, to submit a summary of their research to the blog...">
    <meta property="og:image" content="https://blog.torproject.org/anti-censorship-domain-shadowing/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/anti-censorship-domain-shadowing/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Domain Shadowing: Leveraging CDNs for Robust Blocking-Resistant Communications
      </h1>
    <p class="meta">by mingkui | April 23, 2021</p>
    <picture>
      
      <img class="lead" src="lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p><em>We invited guest blog author, Mingkui Wei, to submit a summary of their research to the blog this week. This blog post is based on the upcoming Usenix Security paper (<a href="https://www.usenix.org/conference/usenixsecurity21/presentation/wei">full version here</a>). Note that the domain shadowing ideas presented herein are intended to be a building block for a future system that doesn't exist for end-users yet. We hope this post will help system designers to think in new ways, and use those ideas to build new censorship circumvention tools.</em></p>
<p><strong>What is Domain Shadowing?</strong><br />
Domain shadowing is a new censorship circumvention technique that uses Content Distribution Networks (CDNs) as its leverage to achieve its goal, which is similar to domain fronting. However, domain shadowing works completely differently from domain fronting and is stronger in terms of blocking-resistance. Compared to domain fronting, one big difference among many is that the user in domain shadowing is in charge of the whole procedure. In other words, the complete system can be solely configured by the user without necessary assistance from neither the censored website nor an anti-censorship organization.</p>
<p><strong>How Domain Shadowing Works</strong><br />
We start this section by explaining how domain names are resolved and translated by CDN.</p>
<p>CDNs act like a reverse proxy that hides the back-end domain and presents only the front-end domain to the public. CDNs typically take two approaches to accomplish the name translation, as shown in the following two figures. We make the following assumptions to facilitate the illustration: assume the publisher's (i.e. the person who wants to use CDN to distribute the content of their website) origin server is hosted on Amazon Web Service (AWS) and assigned a canonical name abc.aws.com, and the publisher wants to advertise the website using the domain example.com, which is hosted on GoDaddy's name server.</p>
<p>Figure 1 shows the name translation procedure used by most CDNs, and we use Fastly as an example. To use Fastly's service, the publisher will first log into their Fastly account and set example.com as the frontend, and abc.aws.com as backend. Then, the publisher will create a new CNAME record in their GoDaddy's name server, which resolves the domain example.com to a fixed domain global.ssl.fastly.net. The remaining steps in Figure 1 are intuitive.</p>
<p>There are also some CDNs who host their own TLD name server, such as Cloudflare. If this is the case, step 2 and step 3 in Figure 1 can be skipped (as shown in Figure 2).</p>
<p>Note that the last four steps on both figures show the difference of how a name resolution is conducted by CDN. Specifically, a regular DNS server will only respond to a DNS query with the location of the origin server and the document must be fetched by the client itself, while the CDN will actually fetch the web document for the client.</p>
<p><img alt="Name resolution by Fastly" src="/static/images/blog/inline-images/figure1.png" /></p>
<p><em>Figure 1. Name resolution by Fastly</em></p>
<p><img alt="Name resolution by Cloudflare" src="/static/images/blog/inline-images/figure2.png" /></p>
<p><em>Figure 2: Name resolution by Cloudflare</em></p>
<p>Based on the above introduction, we can now present how domain shadowing works. Domain shadowing takes advantage of the fact that when the domain binding (i.e. the connection between the frontend and the backend domains) is created, the CDN allows arbitrary domains to be set at the backend. As a result, a user can freely bind a frontend domain to any backend domain. To access a blocked domain (e.g. censored.com) within a censored area, a censored user only needs to take the following steps:</p>
<ol>
<li>The user registers a random domain as the "shadow" domain, for example: shadow.com. We assume the censor won't block this newly registered domain.</li>
<li>The user subscribes to a CDN service that is accessible within the censored area, but the CDN itself is not censored. A practical example would be the CDN deploys all its edge servers outside the censored area.</li>
<li>The user binds the shadow domain to the censored domain in the CDN service by setting the shadow domain as the frontend and the censored domain as the backend.</li>
<li>The user creates a rule in their CDN account to rewrite the Host header of incoming requests from Host:shadow.com to Host:censored.com. This is an essential step since otherwise, the origin server of censored.com will receive an unrecognized Host header and unable to serve the request.</li>
<li>Finally, to access the censored domain, the user sends a request to <a href="https://shadow.com">https://shadow.com</a> within the censored area. The request will be sent to the CDN, which will rewrite the Host header and forwards the request to censored.com. After the response is received from censored.com, the CDN will return the response to the user "in the name" of <a href="https://shadow.com">https://shadow.com</a>.</li>
</ol>
<p>During this process, the censor will only see the user connect to the CDN using HTTPS and request resources from shadow.com, and thus will not block the traffic.</p>
<p>On a CDN that still supports domain fronting, we can apply domain fronting techniques to make domain shadowing stealthier. To do this, we still set shadow.com as the frontend and censored.com as the backend, but when an HTTPS request is issued from within the censored area, the user will request the front domain front.com and set the Host header to be shadow.com. This way, the censor only sees the user is communicating with the front domain and will not even suspect the user's behavior.</p>
<p><strong>What’s the Benefit of Domain Shadowing?</strong><br />
Compared to its siblings, domain fronting, the obvious benefit of domain shadowing is that it can use any CDN (as long as the CDN supports domain shadowing, and based on our experiments most CDNs do) to access any domain. The censored domain does not need to be on the same CDN, which is a big limitation of domain fronting. Actually, the censored domain does not need to be a domain that uses CDN at all. This is a big leap compared to domain fronting, which can only access the domains on the same CDN as the front domain.</p>
<p>Another shortcoming of domain fronting is that it can be (and is being) painlessly disabled by CDNs by mandating the Host header of an HTTPS request must match the SNI of the TLS handshake. Domain shadowing, on the other hand, is harder to be disabled since allowing a user to configure the backend domains is a legitimate feature of CDNs.</p>
<p>Compared to other VPS-based schemes, domain shadowing is (possibly) faster and does not need dedicated third-party support. It is faster because compared to the proxy-on-VPS scheme that uses a self-deployed proxy to relay the traffic, domain shadowing's relay is actually all the CDN's edge servers that operate on the CDN's high-speed backbone network, and the whole infrastructure is optimized specifically to distribute content fast and reliably. The following figure compares the delay of fetching a web document directly from the origin server, using Psiphon, using proxy-over-EC2 (with 2 instances based on different hardware configuration), and using domain shadowing based on 5 different CDN providers (Fastly, Azure CDN, Google CDN, AWS Cloudfront, and StakePath). From the figure, we can see domain shadowing beats other schemes most of the time.</p>
<p><img alt="image: domain shadowing is (possibly) faster" src="/static/images/blog/inline-images/figure3.png" /></p>
<p><strong>Challenges of Domain Shadowing</strong><br />
At this moment, domain shadowing faces the following main challenges:</p>
<p><u>Complexity:</u> The user must config the frontend and backend domains in their CDN account for every censored domain they want to visit. Although such configuration can be automated using the CDN’s API, the user still needs to have sufficient knowledge about relatively complex operations such as how to register to a CDN, enable API configuration and obtain API credentials, and how to register a domain.</p>
<p><u>Cost:</u> Based on our survey, for 500 GB monthly data usage, the cost of using domain shadowing with a reputable CDN is about $40, which increases or decreases linearly with the data usage in general. If the user chooses to use an inexpensive CDN, the cost could be brought to under $10 per month. However, this still can't beat free tools such as Psiphon and Tor.</p>
<p><u>Security:</u> By using domain shadowing, the browser "thinks" it is only communicating with the shadow domain, while the web documents are actually from all the different censored domains (see the following figure where we visit Facebook using Forbes.com as the shadow domain). Such domain transformation makes the Same-Origin-Policy no longer enforceable. While we can use a browser extension to help with this issue to some extent, the user must be aware and cautious about what websites to visit.</p>
<p><img alt="Facebook screenshot" src="/static/images/blog/inline-images/figure4.png" /></p>
<p><u>Privacy:</u> CDNs intercept all HTTP and HTTPS traffic. That is, when a CDN is involved, the HTTPS is no longer between the client and the origin server but between the client and the CDN edge server. Thus, the CDN is able to view and modify any and all traffic between the user and the target server. While this is very unlikely, especially for large and reputable CDNs, users should be aware of the possibility.</p>
<p><strong>Conclusion</strong><br />
We explained domain shadowing, a new technique that achieves censorship circumvention using CDN as leverage. It differs from domain fronting, but can work hand-in-hand with domain fronting to achieve better blocking-resistance. While significant work is still needed to address all the challenges and make it deployable, we see domain shadowing as a promising technique to achieve better censorship circumvention.</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/circumvention">
          circumvention
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-291648"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291648" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anon (not verified)</span> said:</p>
      <p class="date-time">April 23, 2021</p>
    </div>
    <a href="#comment-291648">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291648" class="permalink" rel="bookmark">Tor bridges using this when?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor bridges using this when?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291660"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291660" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">April 24, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291648" class="permalink" rel="bookmark">Tor bridges using this when?</a> by <span>anon (not verified)</span></p>
    <a href="#comment-291660">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291660" class="permalink" rel="bookmark">Not anytime soon -- it is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not anytime soon -- it is more designed for individuals who set up their own CDN accounts and route their own traffic through them. That is, in its current form it's hard to make it scale to many users in an automated way.</p>
<p>That's why we put the note at the front about how the ideas "are intended to be a building block for a future system that doesn't exist for end-users yet. We hope this post will help system designers to think in new ways, and use those ideas to build new censorship circumvention tools."</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291693"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291693" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Pirateparty Joe (not verified)</span> said:</p>
      <p class="date-time">April 30, 2021</p>
    </div>
    <a href="#comment-291693">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291693" class="permalink" rel="bookmark">Wouldnt it be easier to just…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wouldnt it be easier to just setup a small webhosting account, secure it with free cloudflare and then run a phpproxy on that domain to surf anonymously. This would be cheaper than 40$</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291710"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291710" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anon (not verified)</span> said:</p>
      <p class="date-time">May 02, 2021</p>
    </div>
    <a href="#comment-291710">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291710" class="permalink" rel="bookmark">Using the examples above,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Using the examples above, can't a censor still take down either the Fastly or AWS accounts to render the whole system moot? Perhaps I did not understand what specific type of censorship the scheme protects against.</p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
