<!doctype html>
<html>
<head>
    <title>Strength in Numbers: Library Freedom Is Intellectual Freedom | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Strength in Numbers: Library Freedom Is Intellectual Freedom | Tor Project">
    <meta property="og:description" content="In the United States, our outreach efforts have turned to some of the best defenders of democracy, privacy, and human rights: librarians.">
    <meta property="og:image" content="https://blog.torproject.org/strength-numbers-library-freedom-intellectual-freedom/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/strength-numbers-library-freedom-intellectual-freedom/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Strength in Numbers: Library Freedom Is Intellectual Freedom
      </h1>
    <p class="meta">by alison | December 22, 2018</p>
    <picture>
      
      <img class="lead" src="lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>Photo by <a href="https://unsplash.com/photos/sfL_QOnmy00?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Janko Ferlič</a> on <a href="https://unsplash.com/search/photos/library?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></p>
<p><em>This post is one in a series of blogs to complement our 2018 crowdfunding campaign, Strength in Numbers. Anonymity loves company and we are all safer and stronger when we work together. <a href="https://torproject.org/donate/donate-sin-lf-bp">Please contribute today</a>, and your gift will be matched by Mozilla.</em></p>
<p>Anonymity loves company, but it can be hard to get people to show up to the party. Adoption is an issue for pretty much all free software projects, particularly those that might seem challenging for ordinary users. In the last few years, the Tor Project has tried to address this problem by expanding our outreach efforts. Through our Global South Initiative, we've done amazing work with activists and NGOs around the world.</p>
<p>In the United States, our outreach efforts have turned to some of the best defenders of democracy, privacy, and human rights: librarians. A former librarian myself, I started the <a href="https://libraryfreedomproject.org/">Library Freedom Project </a>(LFP), an initiative under the Tor umbrella, that provides training to librarians to help them become privacy advocates in their local communities.</p>
<p>We first introduced LFP to the Tor community <a href="https://blog.torproject.org/guest-post-library-freedom-project-bringing-privacy-and-anonymity-libraries">in 2015</a>. It was originally focused on providing direct trainings to libraries centered on teaching and using Tor, introducing privacy strategies and tools to the mainstream, and occasionally <a href="https://blog.torproject.org/tor-exit-nodes-libraries-pilot-phase-one">setting up Tor relays inside libraries</a>. LFP <a href="https://www.propublica.org/article/library-support-anonymous-internet-browsing-effort-stops-after-dhs-email">made headlines</a> in that first year. We worked with a library in New Hampshire to set up a Tor exit relay, but the U.S. Department of Homeland Security tried to intervene and shut it down. The community fought, and we won to keep it on. Since then, a number of other libraries have been inspired to <a href="https://twitter.com/torproject/status/709736490708901888">run relays</a> and <a href="https://www.torontopubliclibrary.ca/using-the-library/computer-services/tor-browser-pilot/">install Tor Browser</a> on their computers for the public.</p>
<p><a href="https://twitter.com/flexlibris/status/792070173242986496"><img alt="Library Freedom Project poster " src="/static/images/blog/inline-images/lfp-poster.jpg" width="450" class="align-center" /></a></p>
<p>We knew we were onto something. But LFP's staff was small, and there was no way this method would reach a critical mass of libraries and librarians unless it scaled.</p>
<p>That’s why we decided to turn Library Freedom Project's smaller trainings into a free intensive six-month training course called the Library Freedom Institute (LFI), a partnership between LFP and New York University (NYU). LFI aims to provide everything a librarian needs to <a href="https://blog.torproject.org/new-program-turns-librarians-privacy-advocates">become a privacy advocate</a> in their community and keep the momentum growing. LFI provides readings, practical collaborative assignments, a discussion board for participants to have real-time conversation, an in-person meetup in NYC, and weekly lectures from friendly experts around the privacy scene like Micah Lee, Kade Crockford, April Glaser, and Freddy Martinez. This is a one-of-a-kind opportunity for librarians who want to take their advocacy to the next level.</p>
<p>We wrapped up our first cohort at the beginning of December, and we're proud of our success. Our first graduating class of 13 librarians from around the United States will now become part of the Library Freedom Project, providing outreach and training to communities in diverse parts of the country. And we're excited to announce <a href="https://libraryfreedomproject.org/lfi/">we just opened applications</a> for our 2019 cohort for another 25 participants, which will commence in April.</p>
<p>By the end of 2020, we will have run LFI four times and trained about 85 privacy advocates, creating a multiplier effect that will bring privacy and anonymity to more communities. LFI and the Tor Project value training people who can train others, because there is strength in numbers.</p>
<p>Tell your librarian friends to <a href="https://libraryfreedomproject.org/lfi/">apply for LFI</a>. Though tailored to public and community college librarians, LFI is open to librarians from all types of libraries, and it is completely free.</p>
<p>If you’re not connected to the library community, you can help drive this movement to bring privacy to libraries <a href="https://torproject.org/donate/donate-sin-lf-bp">by making a donation to the Tor Project</a>.</p>
<p><a href="https://torproject.org/donate/donate-sin-lf-bp"><img alt="donate button" src="/static/images/blog/inline-images/tor-donate-button_11.png" class="align-center" /></a></p>
<p>Libraries need to remain safe places to learn and exercise intellectual freedom in private, and they need to adapt digitally, with tools like Tor, to ensure that happens. <a href="https://torproject.org/donate/donate-sin-lf-bp">With your support</a>, more librarians can become privacy advocates and provide communities with a much needed safe haven from surveillance and censorship online. <a href="https://torproject.org/donate/donate-sin-lf-bp">Donate now</a>, and Mozilla will match your donation.</p>
<p> </p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/relays">
          relays
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-279204"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279204" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Jedi2 (not verified)</span> said:</p>
      <p class="date-time">December 28, 2018</p>
    </div>
    <a href="#comment-279204">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279204" class="permalink" rel="bookmark">What about privacy on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about privacy on Facebook.I had 500 facebook bans.All censorship needs to stop on Facebook users deserve privacy so people can sell and make money and be anonymous online</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279210"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279210" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 30, 2018</p>
    </div>
    <a href="#comment-279210">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279210" class="permalink" rel="bookmark">How to setup Tor for oldest…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How to setup Tor for oldest Windows XP? Files "tor.exe" and "tor-gencert.exe" nothing did. (First file just said about Tor running a circuit (which one, if no one Tor-browser window?) and then nothing else.)<br />
No mail.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279218"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279218" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 31, 2018</p>
    </div>
    <a href="#comment-279218">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279218" class="permalink" rel="bookmark">Library use privacy: website…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Library use privacy: website browsing being one part, does LFP deal with libraries collecting and reporting customer's other records?</p>
<p>The weird thing is the choice of library software. While many database solutions exist, it seems 8 out of 10 libraries store their catalog (and the customer profiles) remotely on SirsiDynix sites. Check out your libraries - you can see the SisriDynix URL and regulatory statement at the bottom of screen while browsing the library catalogs online.</p>
<p>Then, the sole company could profile the interests and transactions of most library users across the US, and perhaps internationally. When you check out materials from different libraries, they still report to the same SirsiDynix. It's like the 3rd-party tracking the Tor products fight by using anonymity, only here the system has full personal data in your building profile. Not a concern for most, but consider that even the biometric data can be pulled on demand from the surveillance video feeds.</p>
<p>Perhaps the concerned libraries can be encouraged to more privacy in these aspects, too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279229"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279229" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alison
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">alison</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">alison said:</p>
      <p class="date-time">January 02, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279218" class="permalink" rel="bookmark">Library use privacy: website…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279229">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279229" class="permalink" rel="bookmark">Yes, we do help them make…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, we do help them make better library software choices. It's a challenging problem though. Most libraries don't have the in-house IT expertise to run their own servers, so third party contracts are necessary. There are limited choices for library vendors and few of them share the values that librarians have, plus they are not very transparent about their data practices. We need privacy-centric software solutions that libraries can actually afford (libraries have very small budgets).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279232"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279232" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 02, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to alison</p>
    <a href="#comment-279232">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279232" class="permalink" rel="bookmark">Hi Alison,
I would like to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi Alison,</p>
<p>I would like to expand upon the OP's comment with some information about Tor Project's "home town library" (so to speak), the Seattle Public Library system.  Some years ago SPL hired a Canadian company, Bibliocommons, to manage its collections catalog, patron records (including checkouts and catalog searches), and "book club" discussions with personalized "likes", etc., in the cloud, specifically Amazon EC3 servers, but with the data analyzed and "managed" as a service by Bibliocommons in Toronto (which is also home to CES).  </p>
<p>This means that sensitive information about what the citizens of Seattle are reading/thinking is passing (apparently unencrypted) across the US-CA border where apparently both NSA and CES can intercept and store it.  And there is little reason do doubt that they do just that, not because they have a good reason, but just because they can.  </p>
<p>(Modern intelligence agencies are information hoarders: any bit of information might prove useful somehow sometime in the future, they believe, so they collect it all and store it all, forever, because it has become technologically and economically feasible to do so.)</p>
<p>Over the years SPL has also been quite hostile (especially in comparison to the King County Libraries system, for example) toward free speech, blocking not only Tor Project but many new sites (years before the new EU laws came into effect).</p>
<p>Many other public libraries in North America (Canada, US, Mexico) also use Bibliocommons to manage their on-line catalogs and patron records.  One consequence of this is that should FBI desire to examine someone's reading habits, instead of pushing out an administrative subpoena to an individual library (which might very well fight in the courts to protect their patron's privacy), they can address a blanket subpoena to Bibliocommons for a large fraction of library patron records in North America.  In particular, last I checked, New York Public Library (the largest public library system in North America, if not the world) also uses Bibliocommons to manage its catalogs and patron records.</p>
<p>A telling quotation from the founder and CEO of Bibliocommons, Beth Jefferson, speaking to a business audience about the "privacy controls" Bibliocommons offered to patrons:</p>
<p>&gt; At first we went out to users with an interface specifying "public" or "private" nature of their contribution, but most people wanted to keep things private, so we had to reverse the incentives, and shifted the semantics to "share" vs. "hide".</p>
<p>The reason Bibliocommons feels it needs to share patron data--- I am not sure whether they still bother to ask permission--- is that its business model apparently relies on selling patron data.  (Bibliocommons was founded as a nonprofit but later became a profit making company, apparently.)</p>
<p>Even more disturbing, much of the personal data SPL holds about patrons concerns data about children, who are encouraged to seek reading and homework help via Bibliocommons managed social media resources.</p>
<p>Another problem is that Bibliocommons does not allow library patrons to prevent anyone else from adding them as a "friend", a security flaw often abused by LEAs and corporate spooks embarked upon a fishing expedition trawling through social media sites (and thanks to Bibliocommons, all SPL patrons are social media users at the Bibliocommons site--- if you have a library card, you have a social media account with them, tied to your real identity).</p>
<p>Further, as we all know (I presume) very serious and essentially unfixable security flaws which particularly affect "cloud computing" were uncovered a year ago, and given such names as Spectre.  While some software mitigations have been implemented, the only genuine fix is for chip makers to design entire new families of CPUs which avoid "speculative execution", and for cloud computing services to replace their servers once the new chips become available.  Unless someone holds their feet to the fire, I doubt that companies like Bibliocommons and SirsiDynix will do this.</p>
<p>If all this is beginning to sound a bit like Facebook, well, I think the resemblance is disturbing--- and the potential for serious abuse of sensitive personal information is all to real.</p>
<p>In my experience in attempting to goad library systems to address potential security and privacy issues, librarians have disappointed me by becoming defensive, and even attempting to dismiss privacy concerns as unimportant--- an attitude at odds with the pro-privacy ethos which you have described.  I hope LFA can work with public library systems to address these issues in a more effective and constructive manner.</p>
<p>Thanks to the OP for speaking up!  It's always good to know that I am not alone in trying to warn about the dangers to patrons of technically/legally insecure library sites (especially with regard to checkouts, catalog searches, and patron contact information).</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-279233"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279233" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 02, 2019</p>
    </div>
    <a href="#comment-279233">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279233" class="permalink" rel="bookmark">OT, but very welcome news: a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OT, but very welcome news: a nice article urging everyone to use Tor every day just appeared in Wired:</p>
<p>wired.com<br />
Tor Is Easier Than Ever. Time to Give It a Try<br />
Lily Hay Newman<br />
1 Jan 2018</p>
<p>&gt; Tor has been relatively accessible for years now, largely because of the Tor Browser, which works almost exactly like a regular browser and does all the complicated stuff for you in the background. But in 2018 a slew of new offerings and integrations vastly expanded the available tools, making 2019 the year to finally try Tor. You may even end up using the network without realizing it.<br />
...<br />
&gt; With all this new private industry collaboration, the Tor Project's Bagueros says she thinks that more people will start using the service and be able to integrate it into their lives. The Tor Project has been working on ways to scale more efficiently in anticipation of eventually needing to meet this higher demand. But it also remains focused on the core concept of Tor as a distributed and decentralized network. "We don’t want any corporations to own a big part of the network," Bagueros says. "So we educate them on how many servers are okay for them to pitch in and if they want to add more they can donate to different nonprofits who run relays so they can still increase the network that way."<br />
&gt;<br />
&gt; The vision of Tor as the underpinning of the entire internet is still probably a long way off, if it can ever happen at all. But the options available to access the Tor network and use it more easily are rapidly expanding. This is the year to try them out.</p>
<p>Thanks to everyone at Tor Project and to all the volunteers for your hard work!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279270"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279270" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>zukabriak (not verified)</span> said:</p>
      <p class="date-time">January 05, 2019</p>
    </div>
    <a href="#comment-279270">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279270" class="permalink" rel="bookmark">by downloading it am I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>by downloading it am I putting my computer in danger or to the face of many virus ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279284"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279284" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alison
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">alison</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">alison said:</p>
      <p class="date-time">January 07, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279270" class="permalink" rel="bookmark">by downloading it am I…</a> by <span>zukabriak (not verified)</span></p>
    <a href="#comment-279284">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279284" class="permalink" rel="bookmark">You can safely download Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can safely download Tor from torproject.org.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
