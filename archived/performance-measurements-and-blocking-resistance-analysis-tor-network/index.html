<!doctype html>
<html>
<head>
    <title>Performance measurements and blocking-resistance analysis in the Tor network | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Performance measurements and blocking-resistance analysis in the Tor network | Tor Project">
    <meta property="og:description" content="The Tor network has grown to more than one thousand relays and millions of casual users over the...">
    <meta property="og:image" content="https://blog.torproject.org/static/images/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/performance-measurements-and-blocking-resistance-analysis-tor-network/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Performance measurements and blocking-resistance analysis in the Tor network
      </h1>
    <p class="meta">by karsten | May 21, 2009</p>
    <picture>
      <source media="(min-width:415px)" srcset="../static/images/lead.webp" type="image/webp">
<source srcset="../static/images/lead_small.webp" type="image/webp">

      <img class="lead" src="../static/images/lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>The Tor network has grown to more than one thousand relays and millions of casual users over the past few years. We are proud of our network's popularity, but with growth has come increasing <a href="https://blog.torproject.org/blog/why-tor-is-slow" rel="nofollow">performance problems</a> and attempts by some countries to block access to the Tor network. In order to address these problems, we need to learn more about the Tor network. In this post, I describe the current state of network measurements in Tor and some proposed additions to help us understand the network better.</p>

<p>Right now, relays, <a href="https://www.torproject.org/bridges" rel="nofollow">bridges</a>, and directories gather the following data for statistical purposes:</p>

<ul>
<li>Relays and bridges count the number of bytes that they have pushed in 15-minute intervals over the past 24 hours. They include these data in extra-info documents that they send to the directory authorities whenever they publish their server descriptor. See Figure 3 in the <a href="http://git.torproject.org/checkout/metrics/master/report/dirarch/dirarch-2009-03-31.pdf" rel="nofollow">analysis of directory archives</a> that shows that roughly half of the available bandwidth capacity is utilized.</li>
<li>Bridges further include a rough number of clients per country that they have seen in the past 48 hours in their extra-info documents. We <a href="http://git.torproject.org/checkout/tor/master/doc/spec/proposals/126-geoip-reporting.txt" rel="nofollow">added this feature</a> in <a href="http://git.torproject.org/checkout/tor/master/ChangeLog" rel="nofollow">version 0.2.0.13-alpha</a> to help us learn when a given country is trying to block connections to bridges. It also lets us understand bridge adoption better: see for an example an analysis of <a href="http://git.torproject.org/checkout/metrics/master/report/bridges/bridges-2009-04-04.pdf" rel="nofollow">bridge users by countries</a>.</li>
<li>Directories since version <a href="http://git.torproject.org/checkout/tor/master/ChangeLog" rel="nofollow">0.2.1.1-alpha</a> can be configured to count the number of clients they see per country in the past 24 hours and to write them to a local file. We have used these data in the past to <a href="http://git.torproject.org/checkout/tor/master/doc/spec/proposals/ideas/xxx-geoip-survey-plan.txt" rel="nofollow">estimate</a> total <a href="http://git.torproject.org/checkout/metrics/master/report/dirreq/directory-requests-2009-04-23.2.pdf" rel="nofollow">numbers</a> and <a href="http://git.torproject.org/checkout/metrics/master/report/dirreq/dirreq-report-2009-04-30.pdf" rel="nofollow">origins of clients</a>.</li>
</ul>

<p>It turns out that we need to learn more about the Tor network to make it more useful for everyone. In particular, we are trying to identify performance bottlenecks and want to be ready to notice if any countries start blocking access to the Tor network. Therefore, I am planning to extend network measurements by the following kinds of data:</p>

<ul>
<li>Entry guards should count the number of clients per country seen in the past 24 hours and include these numbers in their extra-info documents. These data are similar to what bridges already gather about their clients as well as directories if configured accordingly. (Remember, because Tor paths are more than one hop, the entry guard knows you are using Tor but does not know anything about your destinations.) We need country counts from entry guards to learn how many clients are connected to a single entry guard and if there are or start to be any restrictions for clients connecting from specific countries.</li>
<li>Relays should determine statistics about the number of bytes and cells waiting in their local queues and report them to the directory authorities in their extra-info documents. We need to <a href="http://archives.seul.org/or/dev/Apr-2009/msg00007.html" rel="nofollow">learn more</a> about buffer sizes of relays at various loads to identify current and future performance problems and fix them.</li>
<li>Exit nodes should include the number of bytes and streams they pushed over the past 24 hours broken down by exiting port in their extra-info documents. These data are important for us to identify load-balancing problems with respect to <a href="https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#RunARelayBut" rel="nofollow">exit policies</a>.</li>
</ul>

<p>These approaches have been designed so that none of the network data can be used to deanonymize our users. All network data are aggregated before being uploaded to the directory authorities: client addresses are resolved to countries, added up over at least 24 hours, and rounded up to the next multiple of a fixed number (currently 8). All network data should be made available via the directories just as the current statistical data can be obtained from downloading extra-info documents. The details of the network measurements as outlined here will be specified in <a href="http://git.torproject.org/checkout/tor/master/doc/spec/proposals/001-process.txt" rel="nofollow">proposals</a> and discussed on the <a href="http://archives.seul.org/or/dev/" rel="nofollow">developer mailing list</a>. You can check out our results on the <a href="https://www.torproject.org/projects/metrics" rel="nofollow">metrics project page</a> as we make progress.</p>

<p>We are excited to finally start tackling the performance problems and to prepare ourselves to notice when countries start blocking access to the Tor network. We would love to have help from the rest of the research community in discussing safe ways to measure the described network data and to analyze them later on.</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/circumvention">
          circumvention
        </a>
      </li><li>
        <a href="../category/network">
          network
        </a>
      </li><li>
        <a href="../category/research">
          research
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-1368"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1368" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 21, 2009</p>
    </div>
    <a href="#comment-1368">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1368" class="permalink" rel="bookmark">It may just be my computer,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It may just be my computer, but I'm in China and am at the moment unable to connect to the Tor network.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1369"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1369" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 21, 2009</p>
    </div>
    <a href="#comment-1369">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1369" class="permalink" rel="bookmark">I cannot connect to the Tor Network</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It worked well up until today. I am now receiving a message in the message log which states: </p>
<p>"May 22 18:21:09.015 [Warning] No specified exit routers seem to be running, and StrictExitNodes is set: can't choose an exit."</p>
<p>Can anyone assist please?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1370"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1370" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 21, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1369" class="permalink" rel="bookmark">I cannot connect to the Tor Network</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1370">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1370" class="permalink" rel="bookmark">Re: I cannot connect to the Tor Network    </a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Check out<br />
<a href="https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#ChooseEntryExit" rel="nofollow">https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#ChooseEntryEx…</a></p>
<p>Especially the part where we suggest that you not use ExitNodes and<br />
StrictExitNodes. They're easy to screw up and leave yourself in a broken<br />
state.</p>
<p>So, "stop doing that" would be my suggestion. In the future we hope to<br />
have a better interface for choosing your path (and/or exit) by country.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1371"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1371" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 22, 2009</p>
    </div>
    <a href="#comment-1371">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1371" class="permalink" rel="bookmark">Intrusion of privacy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What are you doing to increase security when nodes are chosen in anti privacy country's like Germany that starting on 1/1-2009 started to log information about from and to IPs and even worse 1/1-2009 Swedish espionage organization(FRA) started collecting &amp; analysing a copy of all the data that passes the Swedish border !!!</p>
<p>Do you plan to use 4 nodes in these circuits in the future with a maximum of 1 node in these country's or how do you withstand these intrusions of the personal integrity these country's started with ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1380"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1380" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 22, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1371" class="permalink" rel="bookmark">Intrusion of privacy</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1380">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1380" class="permalink" rel="bookmark">re:  Intrusion of privacy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For the record, 1/1/2009 was nearly 6 months ago.  We hope you're using secure protocols through Tor such that the risks are lessened.  Someone watching any node will only see you're using Tor, or what IP address you're visiting from an exit node.  If an entry node, they'll know your IP.  If an exit node, they'll know where you are going.  In either case, they can't put who and what together.</p>
<p>A better question is what to do if your first and last node are in the same country, and I don't think we have an answer for that yet.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1391"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1391" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 23, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-1391">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1391" class="permalink" rel="bookmark">re: re: Intrusion of privacy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"For the record, 1/1/2009 was nearly 6 months ago."<br />
Yes i know that. I only mention this as i understand that Tor has still not made a good solution to solve this problem that needs fixing.<br />
"We hope you're using secure protocols through Tor such that the risks are lessened."<br />
Yes of course i am.</p>
<p>"A better question is what to do if your first and last node are in the same country, and I don't think we have an answer for that yet."</p>
<p>You do know that country's like Germany, USA, Sweden and so on are sending these data to the other country's even if it's against there own laws so stopping circuits with 3 nodes in the same country(does this work yet, it wasn't before) or even first &amp; last node as you mention is not enough as a measure against these data retention &amp; collecting they have started with.<br />
Tor's circuits needs to have at least 1 entry or exitnode out of there reach to be a safe solution.</p>
<p>If you would allow a maximum of 1 node from this group of country's who do this kind of things it would of cource improve security a lot for they who themselfs are not able to recofigure there torrc-file.<br />
I understand that this could be a problem because a country like Germany has such a large portion of the Tor networks total bandwidth.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-1406"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1406" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 24, 2009</p>
    </div>
    <a href="#comment-1406">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1406" class="permalink" rel="bookmark">Cannot connect here in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Cannot connect here in Okinawa. Kinda irritating really.</p>
<p>Potentially could be wifi router settings blocking specific ports, but is highly unlikely.</p>
<p>Issue remains the same.</p>
<p>[Notice] We're missing a certificate from authority with signing key [gibberish key removed]: launching request.<br />
[Notice] We're missing a certificate from authority with signing key [gibberish key removed]: launching request.<br />
[Notice] We're missing a certificate from authority with signing key [gibberish key removed]: launching request.<br />
[Warning] 0 unknown, 3 missing key, 2 good, 0 bad, 0 no signature, 4 required</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1408"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1408" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  karsten
  </article>
    <div class="comment-header">
      <p class="comment__submitted">karsten said:</p>
      <p class="date-time">May 25, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1406" class="permalink" rel="bookmark">Cannot connect here in</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1408">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1408" class="permalink" rel="bookmark">My first thought from the &quot;3</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>My first thought from the "3 missing key" part is that you are using a very old Tor version. Which Tor version is this? And can you post the [gibberish key removed] parts, too? There's nothing secret in them, and maybe they help us resolve the problem.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1793"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1793" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Stolen From (not verified)</span> said:</p>
      <p class="date-time">July 12, 2009</p>
    </div>
    <a href="#comment-1793">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1793" class="permalink" rel="bookmark">Thieves</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Your program is actively being used to steal my internet band width to the point that we cannot operate our business.</p>
<p>I now have a timeswitch on the modem so the internet only runs during working hours.</p>
<p>I need you to provide me a program so I have control over the service that I am paying for.</p>
<p>Just remember this if you are not doing something wrong then you dont need to hide what you are doing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1803"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1803" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">July 12, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1793" class="permalink" rel="bookmark">Thieves</a> by <span>Stolen From (not verified)</span></p>
    <a href="#comment-1803">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1803" class="permalink" rel="bookmark">re: thieves</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Here's the email I sent you earlier:</p>
<p>Hello,</p>
<p>Thanks for contacting us.  Did you install Tor?  If so, Tor has many<br />
ways to control the amount of bandwidth consumed.  Many users donate<br />
their unused bandwidth to help others who need their privacy and<br />
anonymity online.</p>
<p>&gt; &gt; I need you to provide me a program so I have control over the service<br />
&gt; &gt; that I am paying for.</p>
<p>We can't provide you a program to control your ISP.  If you can tell me<br />
what you installed, assuming it's tor, I can help you from there.</p>
<p>&gt; &gt; Just remember this if you are not doing something wrong then you<br />
&gt; &gt; don't need to hide what you are doing.</p>
<p>This is a false dichotomy.  There are many reasons to protect your<br />
communications, or to provide some control over who collects information<br />
about you on the Internet without your permission.<br />
<a href="https://torproject.org/torusers" rel="nofollow">https://torproject.org/torusers</a> provides a few examples of users who<br />
need their communications protected.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1804"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1804" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous Tor Person (not verified)</span> said:</p>
      <p class="date-time">July 12, 2009</p>
    </div>
    <a href="#comment-1804">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1804" class="permalink" rel="bookmark">Flags missing on Tor Network Map page</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello,</p>
<p>I just installed the new Tor bundle with Firefox, and when I select "View the Network" from the Vidalia Control Panel, none of the country flags appear in the Relay column to the left.</p>
<p>I had the Tor bundle before the recent Vidalia update, and the flags always appeared normally. (I ended up removing that bundle, and replacing it with the brand new one.)</p>
<p>Do you know -- is this a bug, or a temporary error in the network? I would rather not delete this new Vidalia bundle and start all over again, adding the add-ons, etc., if there is a simple fix for it.</p>
<p>With thanks,</p>
<p>ATP</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-3103"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3103" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>hii (not verified)</span> said:</p>
      <p class="date-time">November 13, 2009</p>
    </div>
    <a href="#comment-3103">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3103" class="permalink" rel="bookmark">http://blog.torproject.org/blog/performance-measurements-and-blo</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you, you answered the question I have been searching for which was whether or not to place keywords when blog commenting. <a href="http://www.hayda.net" rel="nofollow">mirc</a> . <a href="http://www.hayda.net" rel="nofollow">chat</a> . <a href="http://www.hayda.net/" rel="nofollow">http://www.hayda.net/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4131"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4131" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 26, 2010</p>
    </div>
    <a href="#comment-4131">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4131" class="permalink" rel="bookmark">Everything worked fine, till</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Everything worked fine, till yesterday. Now I cant connect and keep getting these messages:<br />
Jan 26 12:36:19.710 [Notice] We're missing a certificate from authority with signing key 665711AF821C459DC59A8491FAD1B9D7A7800ECF: launching request.<br />
Jan 26 12:36:19.711 [Notice] We're missing a certificate from authority with signing key 6584DF098CFC68ACBF5E551532C8A58674586820: launching request.<br />
Jan 26 12:41:24.818 [Notice] We're missing a certificate from authority with signing key 665711AF821C459DC59A8491FAD1B9D7A7800ECF: launching request.<br />
Jan 26 12:41:24.820 [Notice] We're missing a certificate from authority with signing key 6584DF098CFC68ACBF5E551532C8A58674586820: launching request.<br />
Jan 26 12:41:25.081 [Warning] TLS error: unexpected close while renegotiating</p>
<p>What do I do?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4138"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4138" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 26, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4131" class="permalink" rel="bookmark">Everything worked fine, till</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4138">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4138" class="permalink" rel="bookmark">I am having the same issue,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am having the same issue, it just started happening today for pretty much no reason at all.  How can we fix this?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-9023"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9023" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 01, 2011</p>
    </div>
    <a href="#comment-9023">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9023" class="permalink" rel="bookmark">I have the same problem!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have the same problem!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12363"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12363" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 28, 2011</p>
    </div>
    <a href="#comment-12363">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12363" class="permalink" rel="bookmark">For this Error/Notice
We&#039;re</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For this Error/Notice</p>
<p>We're missing a certificate from authority with signing </p>
<p>Update you Tor, cheers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12364"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12364" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 28, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12363" class="permalink" rel="bookmark">For this Error/Notice
We&#039;re</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12364">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12364" class="permalink" rel="bookmark">No, that notice is fine and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, that notice is fine and normal.</p>
<p>It's the "[Warning] TLS error: unexpected close while renegotiating" that means you should upgrade.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
