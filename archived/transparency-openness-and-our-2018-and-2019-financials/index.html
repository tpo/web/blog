<!doctype html>
<html>
<head>
    <title>Transparency, Openness, and Our 2018 and 2019 Finances | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Transparency, Openness, and Our 2018 and 2019 Finances | Tor Project">
    <meta property="og:description" content="After completing standard audits for 2017-2018 and for 2019, our federal tax filings and audits are available. We publish all of our related tax documents for transparency.">
    <meta property="og:image" content="https://blog.torproject.org/transparency-openness-and-our-2018-and-2019-financials/lead.jpg">
    <meta property="og:url" content="https://blog.torproject.org/transparency-openness-and-our-2018-and-2019-financials/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Transparency, Openness, and Our 2018 and 2019 Finances
      </h1>
    <p class="meta">by arma | November 19, 2020</p>
    <picture>
      
      <img class="lead" src="lead.jpg">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>After completing standard audits for 2017-2018 and for 2019, our federal<a href="https://www.torproject.org/about/financials"> tax filings and audits</a> are available. We publish all of our related tax documents for<a href="https://blog.torproject.org/category/tags/form-990"> transparency</a>.</p>
<p>Specifically, there are four new documents:</p>
<ul>
<li>The 2018 Form 990 (our tax document), covering January through June of 2018.</li>
<li>The 2018 Financial statements and audit results, covering January 2017 through June 2018.</li>
<li>The 2019 Form 990, covering July 2018 to June 2019.</li>
<li>The 2019 Financial statements and audit results, covering July 2018 to June 2019.</li>
</ul>
<p>The reason these documents are no longer tied to calendar years is because in 2017 we changed our fiscal year to be "July through June", since having our fiscal year end right in the middle of fundraising season (Dec 31) makes it harder to plan budgets.</p>
<p>Remember that transparency for a privacy project is not a contradiction: privacy is about choice, and we choose to publish all of these aspects of our work in order to build a stronger community. Transparency is about where the money comes from, but it's also about what we do with it: we show you all of our projects, in<a href="https://gitweb.torproject.org/"> source code</a>, and in periodic project and team<a href="https://lists.torproject.org/pipermail/tor-project/"> reports</a>, and in collaborations with<a href="https://blog.torproject.org/tor-heart-pets-and-privacy-research-community"> researchers</a> who help assess and improve Tor. Transparency also means being clear about our values, promises, and priorities as laid out in our<a href="https://gitweb.torproject.org/community/policies.git/plain/social_contract.txt"> social contract</a>.</p>
<p>The board has also continued to publish<a href="https://gitweb.torproject.org/company/policies.git/plain/minutes/"> minutes</a> for seven board meetings in 2018, six meetings in 2019, and three so far in 2020.</p>
<p>The last few years have been a bit bumpy financially, because we grew to be able to cover more of the Tor ecosystem (which was great), but our expenses grew faster than our income (not so great). That approach is sustainable for a while by spending reserves, but once you're out of reserves the remaining option is to solve it by reducing expenses. You can see part of that arc in the 2017-2019 documents here, and the other half of the arc (the more cheerful half) will be included in the 2020-2021 documents.</p>
<p>One contributing factor to the bumpiness is that we didn't expand our grant writing in 2017 as much as (in retrospect) we should have. This challenge makes it even clearer why being dependent on a small set of funding sources impacts our robustness — a few big grant proposals getting rejected rather than accepted was the difference for 2018. Further amplifying the challenge is that for many funders, the time between proposal and decision can be a full year or more, which makes planning ahead both harder and essential.</p>
<p>Some observations to help you read through the 2018 and 2019 financial documents:</p>
<ul>
<li>Tor's revenue for the half-year of 2018 was $1.76 million, and for FY2019 was a bit under $4.9 million. So things have continued to grow, and the hard part is to make sure that revenue and expenses grow together.</li>
<li>2019 marks the first year since 2005 (before Tor was incorporated as a non-profit) where more than half our support came from sources other than the US government. In terms of percentages, 2015-2016-2017 were 85%, 76%, and 51% US government sources respectively. For the half-year 2018 it went back up to 70%, but for 2019 the fraction of our funding that came from US government sources dropped all the way to 42%. We should expect this percentage to keep going up and down in the future, but one of our priorities remains to continue working to reduce our reliance on US government sources.</li>
<li>We also had the highest contributions ever from individuals—$577k—due to the hard work of our fundraising team. Thank you to the broader Tor community for the support! This support is especially valuable because unrestricted donations let us work on the topics and projects that are most important at the time.</li>
<li>Remember the big picture though: Tor's budget remains modest considering the number of people involved and the impact we have. And it is dwarfed by the budgets that our adversaries are spending to make the world a more dangerous and less free place.</li>
<li>Check out the comment sections on the<a href="https://blog.torproject.org/category/tags/form-990"> previous posts</a> for previous years' versions of the usual "omg government funding" and "omg transparency" discussions. You might find<a href="https://blog.torproject.org/blog/transparency-openness-and-our-2014-financials#comment-151396"> this comment</a> more useful than the rest.</li>
<li>When people ask me about Tor funding, I explain that we have four categories of funders: (A) Research funding from groups like the National Science Foundation to do<a href="https://blog.torproject.org/tors-open-research-topics-2018-edition"> fundamental research</a> on privacy and censorship, including studying how to improve Tor's performance and safety, and inventing new censorship circumvention techniques. (B) R&amp;D funding from groups like OTF and DARPA to actually build safer tools. Different funders might have different audiences in mind when they help us make Tor Browser safer and easier to use, but they want the same things out of Tor Browser: in all cases we make all of our work public, and also remember that<a href="https://blog.torproject.org/strength-numbers-why-every-dollar-counts"> anonymity loves company</a>. (C) Deployment and teaching funding from organizations like the US State Dept and Sweden's foreign ministry to do in-country security trainings, user-oriented documentation, and otherwise help activists around the world learn how to be safer on the internet. (D) Core organizational support, primarily from<a href="https://donate.torproject.org/"> individual donations</a> (that's you!), to cover the day-to-day operations of the non-profit, and most importantly to let us spend time on critical tasks that we can't convince a funder to care enough about.</li>
<li>More generally, I should take a brief moment to explain how funding proposals work, for those who worry that governments come to us wanting to pay us to do something bad. The way it works is that we try to find groups with funding for the general area that we want to work on, and then we go to them with a specific plan for what we'd like to do and how much it will cost, and if we're lucky they say ok. There is never any point where somebody comes to us and says "I'll pay you $X to do Y."</li>
<li>In half-of-2018 and 2019 we counted $216k and $738k in "donated services," that is, volunteers helping with translations, website hosting, and contributed patches. Thank you!</li>
<li>The 990 forms have a "Schedule B Contributors" list, which is standard practice for the accountants to anonymize (in case some contributors want to stay anonymous). Here's how they match up to funder names: contributors #1-5 in 2018 correspond to DRL, NSF part one, NSF part two, a grant via NYU for the<a href="https://libraryfreedomproject.org/"> Library Freedom Project</a>, and Rose Foundation. And contributors #1-5 in 2019 correspond to Mozilla, DRL, NSF, Sida, and the Handshake Foundation.</li>
</ul>
<p>In closing, remember that there are many different ways to get involved with Tor, and we need your help. For example, you can <a href="https://donate.torproject.org/">donate</a>, <a href="https://community.torproject.org/">volunteer</a>, and<a href="https://community.torproject.org/relay/"> run a Tor relay</a>. Now is a great time to make a contribution and <a href="https://blog.torproject.org/use-a-mask-use-tor">join our year end campaign and help us resist the surveillance pandemic</a>. Give today, and Friends of Tor will match your donation. Double your impact by making a gift now, and remember, <strong>Use a Mask, Use Tor.</strong> </p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/financials">
          financials
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-290451"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290451" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 19, 2020</p>
    </div>
    <a href="#comment-290451">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290451" class="permalink" rel="bookmark">That&#039;s good to know!
one of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's good to know!</p>
<blockquote><p>one of our priorities remains to continue working to reduce our reliance on US government sources.</p></blockquote>
<p>It's good practice to state what acronyms mean before their usage, had to look these up, hopefully they're correct.</p>
<p><a href="https://en.wikipedia.org/wiki/New_York_University" rel="nofollow">NYU</a><br />
<a href="https://www.opentech.fund/" rel="nofollow">OTF</a><br />
<a href="https://en.wikipedia.org/wiki/Bureau_of_Democracy,_Human_Rights,_and_Labor" rel="nofollow">DRL</a></p>
<p>Seems a lot of money going around! Though I know from experience the arduous task of software development.</p>
<p>Still waiting on comments made on recent blog posts to be posted, guess that doesn't fit within the budget? (Community) Regardless, Thank you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290452"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290452" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 19, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290451" class="permalink" rel="bookmark">That&#039;s good to know!
one of…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-290452">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290452" class="permalink" rel="bookmark">Yep! Check out this page for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yep! Check out this page for many more details about our sponsors:<br />
<a href="https://www.torproject.org/about/sponsors/" rel="nofollow">https://www.torproject.org/about/sponsors/</a></p>
<p>And in fact, for more than just acronym expansion, check out this page that Gaba made for our current work:<br />
<a href="https://gitlab.torproject.org/tpo/team/-/wikis/sponsors-2020" rel="nofollow">https://gitlab.torproject.org/tpo/team/-/wikis/sponsors-2020</a><br />
The old "what we're actually doing" sponsors pages are still on trac, and haven't been migrated to gitlab yet:<br />
<a href="https://trac.torproject.org/projects/tor/wiki/org/sponsors" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/org/sponsors</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290470"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290470" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>DissidentDonor (not verified)</span> said:</p>
      <p class="date-time">November 22, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290451" class="permalink" rel="bookmark">That&#039;s good to know!
one of…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-290470">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290470" class="permalink" rel="bookmark">&gt; It&#039;s good practice to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; It's good practice to state what acronyms mean before their usage</p>
<p>Plus one.</p>
<p>&gt; OTF</p>
<p>Pretty sure that does refer to Open Technology Fund.  And some news which curiously went unreported in the Tor blog back in Jun 2020 seems very relevant to the sticky issue of over-reliance upon dubious (icky/stringy/unreliable) grants to Tor Project:</p>
<p>npr.org<br />
Trump's New Foreign Broadcasting CEO Fires News Chiefs, Raising Fears Of Meddling<br />
David Folkenflik<br />
18 Jun 2020</p>
<p>&gt; President Trump's pick to lead the U.S. Agency for Global Media, Michael Pack, showed up to work Wednesday for the first time after being approved by the U.S. Senate two weeks earlier.  His words to staff were affirming. His actions were anything but. Pack swiftly sidelined most of the agency's senior leadership by stripping them of their authority. He also fired the chiefs of the government-sponsored broadcast networks for foreign audiences that his agency oversees, including Radio Free Europe/Radio Liberty; Radio Free Asia; Office of Cuba Broadcasting, which oversees Radio and Television Martí; and Middle East Broadcasting Networks, which runs Alhurra and Radio Sawa. The two top officials at Voice of America resigned days earlier in anticipation of Pack's arrival. Pack dissolved advisory boards over each of the networks and placed his own aides above them. He gave no reason for his actions other than his authority to do so, according to two people with direct knowledge of the day's events.<br />
&gt; ...<br />
&gt; Pack's bold moves took people who work at the agency by surprise. A memo obtained by NPR that was sent out by Pack's new chief of staff, Emily Newman, said that officials could take no actions and make no external communications without approval from above.  Several staffers, who spoke to NPR on condition they not be named, said that effectively shut down the agency.<br />
&gt; ...<br />
&gt; Pack is a past president of the conservative Claremont Institute and has worked on projects with former Trump political strategist Stephen Bannon, the former executive chairman of the Breitbart News Network.<br />
&gt; ...<br />
&gt; Libby Liu, a past president of Radio Free Asia, had given notice she would step down in July as head of the agency's Open Technology Fund, intended to promote Internet access around the world. Pack made her departure immediate.</p>
<p>So OTF is "effectively shut down" for the forseeable future.</p>
<p>For reasons which are hard to explain, victorious incoming US Presidents cannot undo last-minute crop-burning actions by losing (retreating) outgoing US Presidents overnight.  The process usually takes more than a year.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290480"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290480" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">November 23, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290470" class="permalink" rel="bookmark">&gt; It&#039;s good practice to…</a> by <span>DissidentDonor (not verified)</span></p>
    <a href="#comment-290480">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290480" class="permalink" rel="bookmark">You can follow the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can follow the happenings to save OTF here: <a href="https://twitter.com/SaveOTF" rel="nofollow">https://twitter.com/SaveOTF</a> and here: <a href="https://saveinternetfreedom.tech/" rel="nofollow">https://saveinternetfreedom.tech/</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290481"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290481" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 23, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290480" class="permalink" rel="bookmark">You can follow the…</a> by <a class="tor" title="View user profile." href="/user/194">Al Smith</a></p>
    <a href="#comment-290481">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290481" class="permalink" rel="bookmark">@ Al Smith:
Great (second)…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ Al Smith:</p>
<p>Great (second) link; thanks!</p>
<p>From an open letter (Nov 2020) found at the second link:</p>
<p>&gt; We are calling on governments, companies, and allies in all sectors to diversify funding to internet freedom projects and technologies, by investing in the brave organizations that allow billions of people to safely browse the internet, free from censorship and surveillance.<br />
&gt; ...<br />
&gt; These developments illustrate the dangers of global initiatives with a single point of failure. The internet freedom community is made up of technologists, journalists, digital security trainers, and policy officers that operate globally; yet many are reliant on OTF’s funding, which receives its financial support from a single government. </p>
<p>Exactly.  Funding diversification matters.  Failure to effectively address this issue amounts to ignoring an existential threat to the continued existence of tools like Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290484"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290484" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">November 23, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290481" class="permalink" rel="bookmark">@ Al Smith:
Great (second)…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-290484">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290484" class="permalink" rel="bookmark">Yes, we agree, diversifying…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, we agree, diversifying our funding sources has been and remains a goal of our fundraising efforts. (You can see that we mention it regularly in a quick search: <a href="https://blog.torproject.org/search/node?keys=%22diversify%22" rel="nofollow">https://blog.torproject.org/search/node?keys=%22diversify%22</a>).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290497"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290497" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 24, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290484" class="permalink" rel="bookmark">Yes, we agree, diversifying…</a> by <a class="tor" title="View user profile." href="/user/194">Al Smith</a></p>
    <a href="#comment-290497">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290497" class="permalink" rel="bookmark">@ Al Smith:
&gt; diversifying…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ Al Smith:</p>
<p>&gt; diversifying our funding sources has been and remains a goal of our fundraising efforts</p>
<p>I'd be even happier if Isabela reiterated this, but thanks much, greatly appreciated.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div></div><a id="comment-290456"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290456" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2020</p>
    </div>
    <a href="#comment-290456">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290456" class="permalink" rel="bookmark">Thank you for anticipating…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for anticipating many of the questions you usually get. Hopefully, this post can be referenced to answer those questions in the future. Now that I think about it, wouldn't it help to put these general answers in support.torproject.org?</p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
