<!doctype html>
<html>
<head>
    <title>Two incentive designs for Tor | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Two incentive designs for Tor | Tor Project">
    <meta property="og:description" content="One big challenge to making Tor fast is providing incentives for users to act as relays. So far...">
    <meta property="og:image" content="https://blog.torproject.org/static/images/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/two-incentive-designs-tor/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Two incentive designs for Tor
      </h1>
    <p class="meta">by arma | January 17, 2009</p>
    <picture>
      <source media="(min-width:415px)" srcset="../static/images/lead.webp" type="image/webp">
<source srcset="../static/images/lead_small.webp" type="image/webp">

      <img class="lead" src="../static/images/lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>One big challenge to making Tor fast is providing incentives for users to act as relays. So far we've been getting more relays by 1) building community through interacting more with relay operators, listing the fast ones prominently in the <a href="http://torstatus.kgprog.com/index.php?SR=Bandwidth&amp;SO=Desc" rel="nofollow">Tor status pages</a>, and generally making it clear that you will make the Tor network better if you do, and 2) making it really easy to configure and run a relay by adding a simple GUI interface in Vidalia and adding UPnP support. But we should also consider more direct incentive approaches, for example where Tor is faster for you if you're a relay.</p>

<p>There are two papers that came out in 2008 that everybody pondering incentives in Tor should read. The first is <a href="http://freehaven.net/anonbib/#incentives-fc10" rel="nofollow">"Building Incentives into Tor"</a>, a tech report I coauthored with Johnny Ngan and Dan Wallach from Rice University (update: now it's an FC 2010 paper). The second is <a href="http://freehaven.net/anonbib/#raykova-pet2008" rel="nofollow">"Payment for Anonymous Routing"</a>, published at <a href="http://petsymposium.org/2008/program.php" rel="nofollow">PETS 2008</a> by Androulaki et al from Columbia University.</p>

<p>The first paper proposes that Tor's directory authorities should spot check relays to make sure they're behaving well, and assign "gold star" flags to the good ones in the networkstatus consensus. Then relays give priority service to connections from people who have gold stars. We ran simulations of the idea for various combinations of users and strategies (selfish, cooperative, adaptive, etc), and showed that in general the performance for gold-star users stays good even with many other users and a heavy traffic load on the network. The other main goal of the design uses an economic argument: not only does it provide the (explicit) incentive to run a relay, but it also aims to grow the overall capacity of the network, so even non-relays will benefit.</p>

<p>However, this incentive design has a serious flaw: the set of gold-star users is public. Over time an attacker can narrow down which relays are always the ones online when a certain activity (e.g. posting to a blog) happens. One fix might be to make the gold star status persist for a few weeks after the relay stops offering service, to dampen out fluctuations in the anonymity sets. But I fear that this narrowing-down attack (also known in research papers as the "intersection attack") is still going to work really well against users who only relay traffic around the times they want good performance.</p>

<p>It would seem that any incentives scheme that treats currently running relays specially will fall prey to this attack. We need to find some way to greatly increase the set of people who might be getting priority service. That's where the Columbia paper comes in: they propose that Tor clients use e-cash (digital coins) to pay for high-priority circuits.  The bulk of the paper is in working out how to both a) make sure users can't cheat too often, and b) make sure relays can't use the payments to trace users. They use a hybrid digital cash design, where clients don't mind identifying themselves to the first hop, but then use anonymous digital cash when paying the later hops in the circuit. Most anonymous credential schemes involve way too much computational overhead, so having one that's more practical is a great step.</p>

<p>Because anybody can buy the digital coins, it's not so easy to build a set of suspects when you see somebody use a high-priority circuit.  Of course, once real money gets involved, things get more complex. One big problem is the bank. Actually building a centralized place where people turn dollars into bits and back is a daunting exercise, and other projects have learned the lesson that it's hard to get right. Plus there are still unsolved anonymity questions -- if we see Alice use a credit card to buy some anonymous coins, and then a few minutes later some anonymous person spends some anonymous coins, what did we learn?</p>

<p>On top of that are the social implications of adding money into the system. Nick keeps reminding me of sociological studies saying that rewarding volunteers with t-shirts makes them feel good about their contribution, whereas rewarding them with a small amount of cash makes them subconsciously start to value their contribution based on the cash you give them. So they're more likely to stop volunteering, as they don't feel their effort is properly appreciated. More details <a href="http://www.congo-education.net/wealth-of-networks/ch-04.htm" rel="nofollow">here</a>, <a href="http://fiveandone.wikispaces.com/file/view/Why+Incentive+Plans+Cannot+Work.pdf" rel="nofollow">here</a>, and <a href="http://www.google.com/search?q=Effects+of+externally+mediated+rewards+on+intrinsic+motivation" rel="nofollow">here</a>. It's hard to say how right this research is, but it seems a rough set of variables to add in if we can avoid it.</p>

<p>Beyond that, paying relays introduces other problems. For one, relays now have new incentives to cheat, or to minimize their traffic costs compared to the payments. How do we achieve a good decentralized network if everybody gravitates to the same cheapest hosting provider? Money can even <a href="http://archives.seul.org/or/talk/Dec-2008/msg00061.html" rel="nofollow">change the legal status of relays</a> in some cases.</p>

<p>So how to proceed? My current idea is a combination of the two designs. The directory authorities give out digital coins in exchange for being a good relay, and the coins can be used to build high-priority circuits. The relays track the coins just enough to prevent too much double-spending (using a coin more than once), and then discard them. Now there is no bank, and no real money involved. It's just a resource management approach.</p>

<p>Will a secondary market appear, where people sell their coins on eBay? Perhaps. Fine with me if so. I think that's a different situation than having the protocol itself designed to transfer dollars from users to relays.</p>

<p>The single-use coins make me uncomfortable, because there's a lot of crypto infrastructure and performance questions in getting all those coins right. Worse, if we're really spending coins for every circuit, we will want to rethink our current "feel free to build a bunch of circuits and just use a few" approach that we're getting even more attached to with <a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/151-path-selection-improvements.txt" rel="nofollow">proposal 151</a>. In my ideal world we could give out coins (credentials) that could be used as much as you like in a given time period, so we don't need the whole anonymous cash infrastructure. But if somebody posts their credential to Slashdot, I want some way either to revoke it and/or to notice and not give that guy any more credentials in the future, and that seems hard. So it looks like it'll be single-use coins or bust.</p>

<p>Of course, lest I appear too optimistic, there are a few more barriers to getting this right. We need to make sure Tor's network design can scale to make use of many more relays. We've been making some progress lately at <a href="https://www.torproject.org/projects/lowbandwidth" rel="nofollow">decreasing the bandwidth required for directory downloads</a>, but <a href="https://www.torproject.org/faq#EverybodyARelay" rel="nofollow">many other aspects of this problem still need to be solved</a>. We also need a better way to actually implement priority circuits: our <a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/111-local-traffic-priority.txt" rel="nofollow">current approach</a> sometimes accidentally gives high priority to other circuits too. Lastly, we might find that per-circuit accounting is not sufficient to handle the load that some users want to put on the network. If so, somebody will need to start doing design and research on per-byte accounting.</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/network">
          network
        </a>
      </li><li>
        <a href="../category/research">
          research
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-540"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-540" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 17, 2009</p>
    </div>
    <a href="#comment-540">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-540" class="permalink" rel="bookmark">On my reading list</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"BitBlender: Light-Weight Anonymity for BitTorrent"</p>
<p><a href="http://freehaven.net/anonbib/#bauer:alpaca2008" rel="nofollow">http://freehaven.net/anonbib/#bauer:alpaca2008</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-541"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-541" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 17, 2009</p>
    </div>
    <a href="#comment-541">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-541" class="permalink" rel="bookmark">Enable relays by default?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why not just go the I2P route and enable relays by default?</p>
<p>Sure, there are downsides to doing this, but are they really greater then having to create an entire complex incentive system like the one you discuss?</p>
<p>I constantly run Tor on my laptop because I like for it to be readily available to me, but I don't run as a relay because I move between several LANs on a daily basis, and I don't want to harm the network.  It is my opinion that the goal should be to make it harmless for people to run relays on computers that aren't guaranteed to have high uptime, and then make that the default.  It seems like this would be a much more elegant solution.</p>
<p>This is just my opinion, of course.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-542"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-542" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 17, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-541" class="permalink" rel="bookmark">Enable relays by default?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-542">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-542" class="permalink" rel="bookmark">Re: Enable relays by default?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It already is harmless to run a relay that isn't up all the time. Tor will<br />
automatically use it for cases that can handle unstable relays, and<br />
won't use it for the cases that need stable relays.</p>
<p>For your more general question, see<br />
<a href="https://www.torproject.org/faq#EverybodyARelay" rel="nofollow">https://www.torproject.org/faq#EverybodyARelay</a><br />
and also Section 4.2 of<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/roadmaps/2008-12-19-roadmap-full.pdf" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/roadmaps/2008-12-19-roadma…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-552"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-552" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 22, 2009</p>
    </div>
    <a href="#comment-552">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-552" class="permalink" rel="bookmark">Is there any option for a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is there any option for a proxy to remain active for a certain period. Like if I want to remain connected to one proxy for 2 minutes or 5 minutes. Can it be set.<br />
loking forward for the answer<br />
Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1331"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1331" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Kilroy (not verified)</span> said:</p>
      <p class="date-time">May 14, 2009</p>
    </div>
    <a href="#comment-1331">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1331" class="permalink" rel="bookmark">Interesting reward option</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You could consider the coins more like an advanced bit bucket, and give them a lifetime of say, 1 week, Being that, the longer you maintain gold start status the more coins you accrue, or have it related to amount of bandwidth transferred,  but also allow some type of tag in router id so that you can have router "groups" this would allow for some interesting options, chiefly being, you could have people with tor processing "groups" to, as an rpg player would say "farm coins" where they will more likely setup a farm because coins have a short lifetime, instead of only contributing when they want high speeds. Setting up access control for this type of group could be tricky but it would also allow them to handle any coin \ cash exchange themselves to sell \ rent out their coins to others. Also you could have coins with a short lifespan and coins with a long lifespan, the latter being more so to add a "ranking" for their farm then for bandwidth, as nothing gets people working harder then competition.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1533"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1533" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>swisstorexit (not verified)</span> said:</p>
      <p class="date-time">June 14, 2009</p>
    </div>
    <a href="#comment-1533">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1533" class="permalink" rel="bookmark">reward</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello,</p>
<p>The idea are interessant but will be not better to have : 1) become reward when give some bandwitch and GB to can be more speed , 2) with coins for peoples who have not the possibility to give enough speed .</p>
<p>I am impatient to can just a bit more speed, the problem are for me , when i use nx client for exemple, i need min 10 KB more to can run in remote...</p>
<p>For the moment i can't Torify it...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1769"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1769" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 07, 2009</p>
    </div>
    <a href="#comment-1769">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1769" class="permalink" rel="bookmark">Stupid</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>People in countries that have more internet infrastructure would always win the gold status.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-7213"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7213" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 10, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1769" class="permalink" rel="bookmark">Stupid</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-7213">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7213" class="permalink" rel="bookmark">is that a bad thing? some</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>is that a bad thing? some people will contribute more to the network. they will be rewarded more. everybody already has free access to tor. some people need better service. why not give them the option of getting better service by making the network better for everybody?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-4306"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4306" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 09, 2010</p>
    </div>
    <a href="#comment-4306">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4306" class="permalink" rel="bookmark">Here&#039;s my idea:
-TorProject</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Here's my idea:<br />
-TorProject has a public key, and they RSA sign the current date/time; Anyone who has paid Torproject can *authenticate* (bear with me).. and obtain the latest key value.</p>
<p>-Due to this authentication, the value should be rounded, e.g. to the nearest hour, etc, so that many people will get the same key, and everyone is (fairly) anonymous.</p>
<p>-People who haven't paid Torproject can't get them directly, but can still sniff values *from the traffic they carry*, the more traffic you carry, the more likely you are to know a recent key.</p>
<p>-So having a recent key doesn't mean you have paid, you might have just sniffed it; but either way, a recent key proves you have been somehow helpful (i.e. if you sniffed it from traffic, then you carried traffic).</p>
<p>-Then, nodes can prioritize traffic based on the presence of a key, and on how recent the key is.</p>
<p>-Note that this still works even if you don't have any money involved, and you just have Torproject giving them out randomly to various nodes which are online.</p>
<p>-And, you can also include other organizations, e.g. some charity might also have a recognized key, and the charity gives out keys, too.</p>
<p>-It's along the lines of digital coins, except that everyone who sees a coin can copy it for themselves, and double spending is ENCOURAGED, but they eventually fade away to become worthless.</p>
<p>-also, by using the time, and RSA signing, it means anyone can verify them, without the need for a centralized server (except for the issuing, which can also be decentralized)</p>
<p>-To stop wide-spread replication (_post your latest value here_) maybe some kind of centralized revocation, etc might work, but it can also be decentralized: every time you see a particular key, it becomes less meaningful (i.e. if you see the 10pm key 100 times, but the 9pm key 3 times, then the 9pm actually becomes more prioritized even though it's older)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4878"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4878" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 29, 2010</p>
    </div>
    <a href="#comment-4878">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4878" class="permalink" rel="bookmark">My primary concern is being</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>My primary concern is being able to successfully and consistently hide my ip address and login information at various sites without the risk that an attacker will be able to steal my information for the purpose of identity theft or any other kind of theft.I bring this up because recently my banking information was stolen and my account cleaned out. I'm not exactly sure how it was done but I suspect spyware was used then it the gathered information to log into my account and perform an illegal transfer. I have since put a block on all transfers but it bothers me greatly that this person was able to steal my information so easily. I wasn't using TOR at the time because it was having trouble connecting to the network and perhaps this theft might not have happened if I had used TOR. What I want to see are faster connections , a debug feature and perhaps a compatability wizard to account for the various operating systems that are currently in use. The bottom line is that I want to be able to completely hide beyond all recognition my identity and any information that could potentially put me at risk for theft of any kind.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-5425"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5425" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 19, 2010</p>
    </div>
    <a href="#comment-5425">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5425" class="permalink" rel="bookmark">Major moment which must not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Major moment which must not be lost from attention -- it is consequences for a physical person, which is an operator of tor exit node.  There are precedents which logically cannot be explained ( <a href="http://news.cnet.com/8301-13739_3-9779225-46.html" rel="nofollow">http://news.cnet.com/8301-13739_3-9779225-46.html</a> and linked article <a href="http://itnomad.wordpress.com/2007/09/16/tor-madness-reloaded/" rel="nofollow">http://itnomad.wordpress.com/2007/09/16/tor-madness-reloaded/</a> ).  But nevertheless they take place and some operators of tor exit nodes will ANSWER by the OWN HIDEs in similar histories.  Which person in right mind will go for it, if he/she knows about possible consequences.  There is such "phenomenon" as lawlessness of guys in blue uniform.  It is not explainable from the point of view of logic and common sense.</p>
<p>While there is no protection from such events, when the operator of tor exit node remains one on one with lawlessness of official force, and he will not gain any help &amp; use in that particular moment in life neither from transparent and honest relationships with the internet provider, nor from constitution or human rights, or other nonsense, there is no special sense to discuss expansion of tor network by means of commoners.</p>
<p>This is a REAL problem.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7254"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7254" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 18, 2010</p>
    </div>
    <a href="#comment-7254">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7254" class="permalink" rel="bookmark">Are any of you aware of the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are any of you aware of the bitcoin project? <a href="http://www.bitcoin.org" rel="nofollow">www.bitcoin.org</a> the idea is anonymous untraceable crypto-cash. So far it's in its infancy but worth thinking about for your digital coins.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9374"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9374" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 24, 2011</p>
    </div>
    <a href="#comment-9374">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9374" class="permalink" rel="bookmark">If you&#039;re going to add</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you're going to add digital cash to Tor, you should check out this software:</p>
<p><a href="https://github.com/FellowTraveler/Open-Transactions/wiki" rel="nofollow">https://github.com/FellowTraveler/Open-Transactions/wiki</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
