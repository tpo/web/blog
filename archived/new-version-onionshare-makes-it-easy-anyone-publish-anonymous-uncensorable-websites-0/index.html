<!doctype html>
<html>
<head>
    <title>New version of OnionShare makes it easy for anyone to publish anonymous, uncensorable websites | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="New version of OnionShare makes it easy for anyone to publish anonymous, uncensorable websites | Tor Project">
    <meta property="og:description" content="Since OnionShare hosts a website on your computer anyway, why not use it to host actual websites?">
    <meta property="og:image" content="https://blog.torproject.org/new-version-onionshare-makes-it-easy-anyone-publish-anonymous-uncensorable-websites-0/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/new-version-onionshare-makes-it-easy-anyone-publish-anonymous-uncensorable-websites-0/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        New version of OnionShare makes it easy for anyone to publish anonymous, uncensorable websites
      </h1>
    <p class="meta">by micah | October 14, 2019</p>
    <picture>
      
      <img class="lead" src="lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p><em>The original version of this post can be found on <a href="https://micahflee.com/2019/10/new-version-of-onionshare-makes-it-easy-for-anyone-to-publish-anonymous-uncensorable-websites/">Micah Lee's blog</a>. </em></p>
<p>I’m excited to announce that OnionShare 2.2 is released! You can download it from <a href="https://onionshare.org/">onionshare.org</a>.</p>
<p>When I first wrote OnionShare in 2014, it let you anonymously and securely send files to people. It worked like this: OnionShare zips up the files, starts a local web server on your computer with a link to this zip file, makes this website accessible as a Tor onion service, and shows you the URL of the web server. You send someone this .onion URL, they load it in Tor Browser (loading the website hosted directly on your computer), and then they can download the zip file. As soon as the download is complete, OnionShare shuts down the web service.</p>
<p>In the years since then it has gotten a whole lot better (largely thanks to a <a href="https://github.com/micahflee/onionshare/wiki/Developing-OnionShare">growing community</a> of volunteer contributors). Instead of just sending files, you can use it to receive files now, allowing you to turn your computer into an <a href="https://micahflee.com/2019/02/onionshare-2/">anonymous dropbox</a>. But it has always worked the same way: hosting an anonymous website locally on your computer. But since OnionShare hosts a website on your computer anyway, why not use it to <em>host actual websites</em>?</p>
<p>In addition to the “Share Files” and “Receive Files” tabs, OnionShare 2.2 introduces the “Publish Website” tab. You drag all of the files that make up your website into the OnionShare window and click “Start sharing.” It will start a web server to host your static website and give you a .onion URL. This website is only accessible from the Tor network, so people will need <a href="https://torproject.org/download">Tor Browser</a> to visit it. People who visit your website will have no idea who you are – they won’t have access to your IP address, and they won’t know your identity or your location. And, so long as your website visitors are able to access the Tor network, the website can’t be censored.</p>
<p>Here are some things to keep in mind about how website publishing in OnionShare works:</p>
<p>If any folder in the website that you’re sharing includes an <span class="geshifilter"><code class="php geshifilter-php">index<span style="color: #339933;">.</span>html</code></span> file, then when someone loads that folder in Tor Browser it will load that html file. If any folder doesn’t include an <span class="geshifilter"><code class="php geshifilter-php">index<span style="color: #339933;">.</span>html</code></span> file, it will show a directory listing instead. So you could, for example, publish a website that’s just a bunch of files without any html, and people who load it in Tor Browser will able to browse your files and folders and download individual files.</p>
<p><img alt="" src="https://micahflee.com/2019/10/new-version-of-onionshare-makes-it-easy-for-anyone-to-publish-anonymous-uncensorable-websites/onionshare-directory-listing.png" /></p>
<p>When sharing something that’s not public, OnionShare now uses HTTP basic authentication. So the URLs that you share look like <span class="geshifilter"><code class="php geshifilter-php">http<span style="color: #339933;">:</span><span style="color: #666666; font-style: italic;">//onionshare:[password]@[address].onion</span></code></span> now. When someone loads the URL in Tor Browser, it will ask them if they want to login first, like this:</p>
<p><img alt="" src="https://micahflee.com/2019/10/new-version-of-onionshare-makes-it-easy-for-anyone-to-publish-anonymous-uncensorable-websites/onionshare-basic-auth.png" /></p>
<p>When they click OK, the URL in the address bar no longer contains the <span class="geshifilter"><code class="php geshifilter-php">onionshare<span style="color: #339933;">:</span><span style="color: #009900;">[</span>password<span style="color: #009900;">]</span></code></span> part, and just looks like a normal website. (This protects against shoulder surfing, where an attacker looks at someone’s screen to see the OnionShare URL and visit it themselves.)</p>
<p><img alt="" src="https://micahflee.com/2019/10/new-version-of-onionshare-makes-it-easy-for-anyone-to-publish-anonymous-uncensorable-websites/onionshare-basic-auth-loaded.png" /></p>
<p>If you want to publish your website for anyone to see, you can always go to settings and enable “public mode”, which simply doesn’t use a username and password anymore.</p>
<p>If you want to use OnionShare to publish a website that you intend to remain online for a long time, it’s important to remember that your computer itself is literally the web server. If you turn off your computer, or even just suspend your laptop, the website will go down. To prevent this, you’ll have to use a computer that’s always turned on for this. You’ll also probably want to go into settings and check “Use a persistent address” – this means that if you close OnionShare and re-open it again (for example, if you have to install updates on the computer and reboot it), the URL will stay the same the next time you start the server. If you don’t use a persistent address, every URL is temporary, and there’s no way to re-use an old URL.</p>
<p>Another thing that’s new is that OnionShare will now show you exactly what web requests people are making to your website (you get to see this when sharing and receiving files too, not just for publishing websites). For example, here’s a website hosted by OnionShare getting scanned with the <a href="https://cirt.net/nikto2">nikto</a> web vulnerability scanner.</p>
<p><img alt="" src="https://micahflee.com/2019/10/new-version-of-onionshare-makes-it-easy-for-anyone-to-publish-anonymous-uncensorable-websites/onionshare-nikto.png" /></p>
<p>And finally, since we put in all of the work to make it so you can browse through directory listings when publishing a website, we also made it so you can similarly browse through folders that are being shared when just sharing files, so people can see exactly what files they’re about to download before downloading them.</p>
<p><img alt="onionshare website mode " src="/static/images/blog/inline-images/onionshare-website-mode.png" class="align-center" /></p>
<p>And if you go into settings and uncheck “Stop sharing after files have been sent” (this is the setting that makes the server shutdown after the first person downloads the files you’re sharing), then people will also be able to download individual files that you’re sharing, instead of only having the option to download everything at once.</p>
<p>I hope you enjoy the new OnionShare!</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/applications">
          applications
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Comments are closed.</p>
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
