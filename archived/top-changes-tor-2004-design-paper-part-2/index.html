<!doctype html>
<html>
<head>
    <title>Top changes in Tor since the 2004 design paper (Part 2) | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Top changes in Tor since the 2004 design paper (Part 2) | Tor Project">
    <meta property="og:description" content="This is part 2 of Nick Mathewson and Steven Murdoch&#39;s series on what has changed in Tor&#39;s design...">
    <meta property="og:image" content="https://blog.torproject.org/static/images/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/top-changes-tor-2004-design-paper-part-2/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Top changes in Tor since the 2004 design paper (Part 2)
      </h1>
    <p class="meta">by nickm | October 22, 2012</p>
    <picture>
      <source media="(min-width:415px)" srcset="../static/images/lead.webp" type="image/webp">
<source srcset="../static/images/lead_small.webp" type="image/webp">

      <img class="lead" src="../static/images/lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>This is part 2 of Nick Mathewson and Steven Murdoch's series on what has changed in Tor's design since the original design paper in 2004.  <a href="https://blog.torproject.org/blog/top-changes-tor-2004-design-paper-part-1" rel="nofollow">Part one is back over here.</a></p>

<p>In this installment, we cover changes in how we pick and use nodes in our circuits, and general anticensorship features.</p>

<h2>5. Guard nodes</h2>

<p>We assume, based on a fairly large body of research, that if an attacker controls or monitors the first hop and last hop of a circuit, then the attacker can de-anonymize the user by correlating timing and volume information. Many of the security improvements to path selection discussed in this post concentrate on reducing the probability that an attacker can be in this position, but no reasonably efficient proposal can eliminate the possibility.</p>

<p>Therefore, each time a user creates a circuit, there is a small chance that the circuit will be compromised. However, most users create a large number of Tor circuits, so with the original path selection algorithm, these small chances would build up into a potentially large chance that at least one of their circuits will be compromised.</p>

<p>To help improve this situation, in Tor 0.1.1.2-alpha, the guard node feature was implemented (initially called "helper nodes", invented by <a href="http://people.cs.umass.edu/~mwright/papers/wright-passive.pdf" rel="nofollow">Wright, Adler, Levine, and Shields</a> and proposed for use in Tor by <a href="http://www.onion-router.net/Publications/locating-hidden-servers.pdf" rel="nofollow">Øverlier and Syverson</a>). In Tor 0.1.1.11-alpha it was enabled by default. Now, the Tor client picks a few Tor nodes as its "guards", and uses one of them as the first hop for all circuits (as long as those nodes remain operational).</p>

<p>This doesn't affect the probability that the first circuit is compromised, but it does mean that if the guard nodes chosen by a user are not attacker-controlled all their future circuits will be safe. On the other hand, users who choose attacker-controlled guards will have about M/N of their circuits compromised, where M is the amount of attacker-controlled network resource and N is the total network resource. Without guard nodes every circuit has a (M/N)2 probability of being compromised.</p>

<p>Essentially, the guard node approach recognises that some circuits are going to be compromised, but it's better to increase your probability of having <b>no</b> compromised circuits at the expense of also increasing the proportion of your circuits that will be compromised if any of them are. This is because compromising a fraction of a user's circuits—sometimes even just one—can be enough to compromise a user's anonymity. For users who have good guard nodes, the situation is much better, and for users with bad guard nodes the situation is not much worse than before.</p>

<h2>6. Bridges, censorship resistance, and pluggable transports</h2>

<p>While Tor was originally designed as an anonymous communication system, more and more users need to circumvent censorship rather than to just preserve their privacy. The two goals are closely linked – to prevent a censor from blocking access to certain websites, it is necessary to hide where a user is connecting to. Also, many censored Internet users live in repressive regimes which might punish people who access banned websites, so here anonymity is also of critical importance.</p>

<p>However, anonymity is not enough. Censors can't block access to certain websites browsed over Tor, but it was easy for censors to block access to the whole of the Tor network in the original design. This is because there were a handful of directory authorities which users needed to connect to before they could discover the addresses of Tor nodes, and indeed some censors blocked the directory authorities. Even if users could discover the current list of Tor nodes, censors also blocked the IP addresses of all Tor nodes too.</p>

<p>Therefore, the <a href="https://svn.torproject.org/svn/projects/design-paper/blocking.html" rel="nofollow">Tor censorship resistance design</a> introduced bridges – special Tor nodes which were not published in the directory, and could be used as entry points to the network (both for downloading the directory and also for building circuits). Users need to find out about these somehow, so the bridge authority collects the IP addresses and gives them out via email, on the web, and via personal contacts, so as to make it difficult for the censor to enumerate them all.</p>

<p>That's not enough to provide censorship resistance though. Preventing the censor from knowing all the IP addresses they need to block to block access to the Tor network will be enough to defeat some censors. But others have the capability to block not only by IP address but also by content (deep packet inspection). Some censors have tried to do this already and Tor has, in response, gradually changed its TLS handshake to better imitate web browsers.</p>

<p>Impersonating web browsers is difficult, and even if Tor perfectly impersonated one, some censors could just block encrypted web browsing (like Iran did, for some time). So it would be better if Tor could impersonate multiple protocols. Even better would be if other people could contribute to this goal, rather than the Tor developers being a bottleneck. This is the motivation of the <a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/proposals/180-pluggable-transport.txt" rel="nofollow">pluggable transports</a> design which allows Tor to manage an external program which transforms Tor traffic into some hard-to-fingerprint obfuscation.</p>

<h2>7. Changes and complexities in our path selection algorithms</h2>

<p>The original Tor paper never specified how clients should pick which nodes to use when constructing a circuit through the network.  This question has proven unexpectedly complex.</p>

<h3>Weighting node selection by bandwidth</h3>

<p>The simplest possible approach to path construction, which we used in the earliest versions of Tor, is simply to pick uniformly at random from all advertised nodes that could be used for a given position in the path.  But this approach creates terrible bandwidth bottlenecks: a server that would allow 10x as many bytes per second as another would still get the same number of circuits constructed through it.</p>

<p>Therefore, Tor 0.0.8rc1 started to have clients weight their choice of nodes by servers' advertised bandwidths, so that a server with 10x as much bandwidth would get 10x as many circuits, and therefore (probabilistically) 10x as much of the traffic.</p>

<p>(In the original paper, we imagined that we might take Morphmix's approach, and divide nodes into "bandwidth classes", such that clients would choose only from among nodes having at least the same approximate bandwidth as the clients.  This may be a good design for peer-to-peer anonymity networks, but it doesn't seem to work for the Tor network: the most useful high-capacity nodes have more capacity than nearly any typical client.)</p>

<p>Later, it proved that weighting by bandwidth was also suboptimal, because of nonuniformity in path selection rules.  Consider that if node A is suitable for use at any point in a circuit, but node B is suitable only as the middle node, then node A will be considered for use three times as often as B.  If the two nodes have equal bandwidth, node A will be chosen three times as often, leading to it being overloaded in comparison with B.  So eventually, in Tor 0.2.2.10-alpha, we moved to a more sophisticated approach, where nodes are chosen proportionally to their bandwidth, as weighted by an algorithm to optimize load-balancing between nodes of different capabilities.</p>

<h3>Bandwidth authorities</h3>

<p>Of course, once you choose nodes with unequal probability, you open the possibility of an attacker trying to see a disproportionate number of circuits -- not by running an extra-high number of nodes -- but by claiming to have a very large bandwidth.</p>

<p>For a while, we tried to limit the impact of this attack by limiting the maximum bandwidth that a client would believe, so that a single rogue node couldn't just claim to have infinite bandwidth.</p>

<p>In 0.2.1.17-rc, clients switched from using bandwidth values advertised by nodes themselves to using values published in the network status consensus document.  A subset of the authorities measure and vote on nodes' observed bandwidth, to prevent misbehaving nodes from claiming (intentionally or accidentally) to have too much capacity.</p>

<h3>Avoiding duplicate families in a single circuit</h3>

<p>As mentioned above, if the first and last node in a circuit are controlled by an adversary, they can use traffic correlation attacks to notice that the traffic entering the network at the first hop matches traffic leaving the circuit at the last hop, and thereby trace a client's activity with high probability.  Research on preventing this attack has not yet come up with any affordable, effective defense suitable for use in a low-latency anonymity network.  Therefore, the most promising mitigation strategies seem to involve lowering the attacker's chances of controlling both ends of a circuit.</p>

<p>To this end, clients do not use any two nodes in a circuit whose IP addresses are in the same /16 – when we designed the network, it was marginally more difficult to acquire a large number of disparate addresses than it was to get a large number of concentrated addresses.  (Roger and Nick may have been influenced by their undergraduacy at MIT, where their dormitory occupied the entirety of 18.244.0.0/16.)  This approach is imperfect, but possibly better than nothing.</p>

<p>To allow honest node operators to run more than one server without inadvertently giving themselves the chance to see more traffic than they should, we also allow nodes to declare themselves to be members of the same "family", such that a client won't use two nodes in the same family in the same circuit.  (Clients only believe mutual family declarations, so that an adversary can't capture routes by having his nodes claim unilaterally to be in a family with every node the adversary <i>doesn't</i> control.)</p>

<h2>8. Stream isolation</h2>

<p>Building a circuit is fairly expensive (in terms of computation and bandwidth) for the network, and the setup takes time, so the Tor client tries to re-use existing circuits if possible, by sending multiple TCP streams down them. Streams which share a circuit are linkable, because the exit node can tell that they have the same circuit ID. If the user sends some information on one stream which gives their identity away, the other streams on the same circuit will be de-anonymized. </p>

<p>To reduce the risk of this occurring, Tor will not re-use a circuit which the client first used more than 10 minutes ago. Users can also use their Tor controller to send the "NEWNYM" signal, preventing any old circuits being used for new streams. As long as users don't mix anonymous and non-anoymous tasks at the same time, this form of circuit re-use is probably a good tradeoff.</p>

<p>However, <a href="http://hal.inria.fr/docs/00/47/15/56/PDF/TorBT.pdf" rel="nofollow">Manils et al.</a> discovered that some Tor users simultaneously ran BitTorrent over the same Tor client as they did web browsing. Running BitTorrent over Tor is a bad idea because the network can't handle the load, and because BitTorrent packets include the user's real IP address in the payload, so it isn't anonymous. But running BitTorrent while doing anonymous web browsing is an especially bad idea. An exit node can find the user's IP address in the BitTorrent payload then trivially de-anonymize all streams sharing the circuit.</p>

<p>Running BitTorrent over Tor is still strongly discouraged, but this paper did illustrate some potential problems with circuit reuse so <a href="https://gitweb.torproject.org/torspec.git/blob/master:/proposals/171-separate-streams.txt" rel="nofollow">proposal 171</a> was written, and implemented in Tor 0.2.3.3-alpha, to help isolate streams which shouldn't share the same circuit. By default streams which were initiated by different clients, which came from SOCKS connections with different authentication credentials, or which came to a different SOCKS port on the Tor client, are separated. In this way, a user can isolate applications by either setting up multiple SOCKS ports on Tor and using one per application, or by setting up a single SOCKS port but using different username/passwords for each application. Tor can also be configured to isolate streams based on destination address and/or port.</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/research">
          research
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-17860"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17860" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 22, 2012</p>
    </div>
    <a href="#comment-17860">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17860" class="permalink" rel="bookmark">Proposal 171 sure is an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Proposal 171 sure is an interesting read, and implements very nice stream separation measures. Looking forward to Tor 0.2.3.3-alpha releasing so I can try them out!<br />
.<br />
Specifically, I want to separate my RSS news reader streams from my TorBrowser streams. Obviously, IsolateDestPort won't work for this.<br />
.<br />
So I'm looking forward to trying out IsolateSOCKSUser option. Then launching the RSS reader with Torsocks and assigning it a randomly generated (UUID), to use as the username for SOCKSPort.  I plan to use the "torlaunch" bash script, listed in proposal 171, to accomplish this. Brilliant!<br />
.<br />
I can't thank everyone enough at Tor Project, for all the hard work they've put into this project. Thank you for sharing your hard work with the rest of the world!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17861"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17861" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 22, 2012</p>
    </div>
    <a href="#comment-17861">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17861" class="permalink" rel="bookmark">Thanks for the great work on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the great work on Tor.<br />
It helps us in china.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17868"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17868" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 22, 2012</p>
    </div>
    <a href="#comment-17868">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17868" class="permalink" rel="bookmark">Any comment on the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Any comment on the implications of this initiative?</p>
<p><a href="https://www.bof.nl/2012/10/18/dutch-proposal-to-search-and-destroy-foreign-computers/" rel="nofollow">https://www.bof.nl/2012/10/18/dutch-proposal-to-search-and-destroy-fore…</a></p>
<p>"If the location of the computer cannot be determined, for example in the case of Tor-hidden services, the police are not required to submit a request for legal assistance to another country before breaking in."</p>
<p><a href="https://www.eff.org/deeplinks/2012/10/dutch-government-proposes-cyberattacks-against-everyone" rel="nofollow">https://www.eff.org/deeplinks/2012/10/dutch-government-proposes-cyberat…</a></p>
<p>"If it became law, this proposal would allow Dutch police to launch direct attacks against international cloud computing services. It would allow Dutch police to use exploits and malware against privacy systems like the Tor network, endangering the hundreds of thousand of people who use Tor clients every day, and those who publish hidden services in the network. It would, in short, allow Dutch police to use the methods of cyberwar to enforce Dutch law on people living anywhere in the world."</p>
<p>If this were enacted, initial targets would no doubt include Tor hidden services hosting content objectionable to someone in the Dutch government.  Next, content offensive to someone in the Syrian government.  Then, content offensive to anyone who hires a "malware as a service" company founded in Holland.</p>
<p>And what about peer to peer services such as i2p which is bundled with Tails?  Isn't it likely that in the near future, clog plods who are fully aware that most users of p2p software are consistently law-abiding citizens might nonetheless break into all their computers for the purpose of searching out and destroying the computers of the persons they suspect might actually have done something wrong?</p>
<p>Civil libertarians are very familiar with the kind of mission creep which has seen laws enacted "to combat terrorism and cyberporn" are quickly exploited to target political bloggers, persons accused of downloading pirated media, persons accused of inadequate effort to recycling, persons who simply happen to live in the "wrong" neighborhoods, and other far less offensive figures than the late Mr. Bin Laden.</p>
<p>Some of us have long urged Tor to modify that part of its mission statement which rules out attempting to defend against "a global adversary".  Because in the modern world, everyone in the world is potentially a target of the new breed of cyberwarriors and paramiitary e-SWAT cybercops.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17871"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17871" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 23, 2012</p>
    </div>
    <a href="#comment-17871">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17871" class="permalink" rel="bookmark">I have apple iphone 3g ,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have apple iphone 3g , where is my tor onion download version??? please help me!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17946"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17946" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 31, 2012</p>
    </div>
    <a href="#comment-17946">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17946" class="permalink" rel="bookmark">Thanks for the great work on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the great work on Tor.<br />
It helps us in the Netherlands.</p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
